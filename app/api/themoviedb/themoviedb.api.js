import { get } from 'lodash'

import doRequest from '../../util/request.util'
import configUtil from '../../util/config.util'
import genresMap from '../../constants/themoviedb-genres'
import { generalLogger } from '../../util/logs'

export class TheMovieDBApi {
  BASE_URL = 'https://api.themoviedb.org/3'

  getPlayingNow = async () => {
    generalLogger.info(`Requesting TheMovieDB Api getPlayingNow endpoint`)
    console.time('themoviedbApi.getPlayingNow')
    const url = this.getUrl('movie/now_playing?region=US')
    const response = await doRequest(this.getDefaultRequestOptions(url))
    const parsedResponse = this.parseResponse(response)
    console.timeEnd('themoviedbApi.getPlayingNow')
    return parsedResponse
  }

  searchMovies = async query => {
    console.time('themoviedbApi.searchMovies')
    const queryEncoded = encodeURIComponent(query)
    generalLogger.info(
      `Requesting TheMovieDB Api Search endpoint with query: ${queryEncoded}`,
    )
    const url = this.getUrl(`search/movie?query=${queryEncoded}`)
    const response = await doRequest(this.getDefaultRequestOptions(url))
    const parsedResponse = this.parseSearchResponse(response)
    console.timeEnd('themoviedbApi.searchMovies')
    return parsedResponse
  }

  parseResponse(response) {
    return this.parseMovies(response.results)
  }

  parseSearchResponse(response) {
    return {
      page: response.page,
      totalResults: response.total_results,
      totalPages: response.total_pages,
      movies: this.parseMovies(response.results),
    }
  }

  parseMovies(movies) {
    return movies.map(apiMovie => ({
      id: apiMovie.id,
      title: apiMovie.title,
      originalTitle: apiMovie.original_title,
      posterPath: apiMovie.poster_path ? apiMovie.poster_path : null,
      genres: this.parseMovieGenres(apiMovie.genre_ids),
      overview: apiMovie.overview,
      releaseDate: apiMovie.release_date,
      year: this.extractYearFromReleaseDate(apiMovie.release_date),
    }))
  }

  parseMovieGenres(genreIds) {
    return genreIds ? genreIds.map(genreId => genresMap[genreId]) : []
  }

  extractYearFromReleaseDate(releaseDate) {
    return releaseDate ? releaseDate.substring(0, releaseDate.indexOf('-')) : ''
  }

  getUrl(path) {
    const apiKey = configUtil.getValue('themoviedb_key')
    if (path.indexOf('?') >= 0) {
      return `${this.BASE_URL}/${path}&api_key=${apiKey}`
    } else {
      return `${this.BASE_URL}/${path}?api_key=${apiKey}`
    }
  }

  getDefaultRequestOptions(url) {
    return {
      url,
      json: true,
    }
  }

  async getOfficialTrailerYoutubeKey(tmdbId) {
    generalLogger.info(`Requesting TheMovieDB Api movie videos endpoint`)
    console.time('themoviedbApi.getOfficialTrailerYoutubeKey')
    const url = this.getUrl(`movie/${tmdbId}/videos`)
    const response = await doRequest(this.getDefaultRequestOptions(url))
    const results = get(response, 'results', [])
    const officialTrailer = results.find(video => {
      const videoName = video.name.toLowerCase()
      return videoName.indexOf('official trailer') > -1
    })
    const finalTrailer = officialTrailer || results[0]
    const officalTrailerKey = get(finalTrailer, 'key')
    console.timeEnd('themoviedbApi.getOfficialTrailerYoutubeKey')
    return officalTrailerKey
  }
}

export default new TheMovieDBApi()
