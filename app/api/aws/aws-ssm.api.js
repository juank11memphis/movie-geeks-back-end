import aws from 'aws-sdk'

import { errorsLogger } from '../../util/logs'

export class AWSParametersStore {
  constructor() {
    aws.config.update({ region: process.env.AWS_REGION })
  }

  getParametersByPathAsPromise = (path, nextToken) => {
    const promise = new Promise((resolve, reject) => {
      const ssm = new aws.SSM()
      let params = {
        Path: path,
        Recursive: true,
        WithDecryption: true,
      }
      if (nextToken) {
        params = {
          ...params,
          NextToken: nextToken,
        }
      }
      ssm.getParametersByPath(params, (err, data) => {
        if (err) {
          errorsLogger.error(
            `Unexpected error loading parameters by path: ${path}`,
            err,
          )
          return resolve({})
        }
        resolve(data)
      })
    })
    return promise
  }

  getParametersByPath = async (path, nextToken = null, acc = []) => {
    try {
      const data = await this.getParametersByPathAsPromise(path, nextToken)
      const parameters = data['Parameters']
      if (data['NextToken']) {
        return await this.getParametersByPath(path, data['NextToken'], [
          ...acc,
          ...parameters,
        ])
      }
      return [...acc, ...parameters]
    } catch (error) {
      return []
    }
  }
}

export default new AWSParametersStore()
