import UserRole from './user-role'

export class UserRolesDao {
  loadByIds(ids) {
    return UserRole.find({ _id: { $in: ids } })
  }
}

export default new UserRolesDao()
