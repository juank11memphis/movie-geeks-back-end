import mongoose from 'mongoose'

const Schema = mongoose.Schema

const UserRolesSchema = new Schema(
  {
    name: String,
    permissions: Array,
  },
  {
    timestamps: true,
    collection: 'user-roles',
  },
)

export default mongoose.models.UserRoles ||
  mongoose.model('UserRoles', UserRolesSchema)
