import { keyBy } from 'lodash'

import userRolesDao from './user-roles.dao'

export class UserRolesService {
  async loadByIds(ids) {
    const userRoles = await userRolesDao.loadByIds(ids)
    const userRolesById = keyBy(userRoles, '_id')
    return ids.map(userRoleId => userRolesById[userRoleId])
  }
}

export default new UserRolesService()
