import { keyBy } from 'lodash'

import usersDao from './users.dao'

export class UsersService {
  async loadByIds(ids) {
    const users = await usersDao.loadByIds(ids)
    const usersById = keyBy(users, '_id')
    return ids.map(userId => usersById[userId])
  }

  create(data) {
    return usersDao.create(data)
  }

  updateUser = async (userId, updateData) => {
    delete updateData.password
    delete updateData.email
    const dbUser = await usersDao.getDetailById(userId)
    await usersDao.update(dbUser, updateData)
    return usersDao.getDetailById(userId)
  }

  getDetailByEmail(email) {
    return usersDao.getDetailByEmail(email)
  }

  getDetailById(userId) {
    return usersDao.getDetailById(userId)
  }
}

export default new UsersService()
