import User from './user'

export class UsersDao {
  loadByIds(ids) {
    return User.find({ _id: { $in: ids } })
  }

  create(data) {
    const newUser = new User(data)
    return newUser.save()
  }

  update(dbUser, updateData) {
    return dbUser.updateOne(updateData)
  }

  getDetailById(id) {
    return User.findById(id)
  }

  getDetailByEmail(email) {
    return User.findOne({ email })
  }
}

export default new UsersDao()
