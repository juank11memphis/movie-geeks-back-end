import mongoose from 'mongoose'

const Schema = mongoose.Schema

const UserSchema = new Schema(
  {
    firstname: {
      type: String,
      required: [true, 'First name is required'],
    },
    lastname: {
      type: String,
    },
    email: {
      type: String,
      unique: true,
      lowercase: true,
      required: [true, 'Email is required'],
      validate: [
        {
          validator(email) {
            // eslint-disable-next-line max-len
            const emailRegex = /^[-a-z0-9%S_+]+(\.[-a-z0-9%S_+]+)*@(?:[a-z0-9-]{1,63}\.){1,125}[a-z]{2,63}$/i
            return emailRegex.test(email)
          },
          message: '{VALUE} is not a valid email',
        },
        {
          isAsync: true,
          validator: (email, callback) => {
            mongoose.models.User.findOne({ email }).then(user => {
              callback(user ? false : true)
            })
          },
          message: 'Email already in use',
        },
      ],
    },
    password: {
      type: String,
      minlength: [6, 'Password must be at least 6 characters long'],
    },
    hasPassword: {
      type: Boolean,
    },
    roles: [
      {
        type: Schema.Types.ObjectId,
        ref: 'UserRoles',
      },
    ],
  },
  {
    timestamps: true,
    collection: 'users',
  },
)

UserSchema.pre('update', function(done) {
  this.options.runValidators = true
  done()
})

export default mongoose.models.User || mongoose.model('User', UserSchema)
