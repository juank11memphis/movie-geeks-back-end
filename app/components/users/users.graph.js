import { AuthenticationError } from 'apollo-server'

export const userType = `
  type User {
    id: String!
    firstname: String
    lastname: String
    email: String
    hasPassword: Boolean
    roles: [UserRole]
  }

  type UserShort {
    id: String!
    firstname: String
    lastname: String
  }

  input UserInput {
    firstname: String
    lastname: String
    email: String
    password: String
  }
`

const loadUserRoles = async (user, loaders) =>
  loaders.userRoles.loadMany(user.roles)

export const userTypeResolver = {
  roles: (user, args, context) => {
    const { loaders } = context
    return loadUserRoles(user, loaders)
  },
}

export const usersQueryTypes = `
  loadAuthUser: User
  loadUserById(userId: String): UserShort
`

export const userQueriesResolvers = {
  loadAuthUser: (root, args, context) => {
    const { usersService, currentUser } = context
    if (!currentUser) {
      throw new AuthenticationError('Unauthorized!!')
    }
    return usersService.getDetailById(currentUser._id)
  },
  loadUserById: (root, args, context) => {
    const { userId } = args
    const { usersService } = context
    return usersService.getDetailById(userId)
  },
}
