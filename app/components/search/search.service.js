import themoviedbApi from '../../api/themoviedb/themoviedb.api'
import moviesService from '../movies/movies.service'

export class SearchService {
  search = async query => {
    if (!query || query === '') {
      return {
        movies: [],
      }
    }
    const searchResult = await themoviedbApi.searchMovies(query)
    const { movies, ...rest } = searchResult
    const dbMovies = await moviesService.saveAllTMDBMovies(movies)
    return {
      movies: dbMovies,
      ...rest,
    }
  }
}

export default new SearchService()
