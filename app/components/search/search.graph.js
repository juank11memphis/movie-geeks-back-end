import { get } from 'lodash'

export const searchType = `
  type Search {
    page: Int
    totalResults: Int
    totalPages: Int
    userMovies: [UserMovie]
  }
`

export const searchQueryTypes = `
  search(query: String): Search
`

export const searchQueriesResolvers = {
  search: (root, args, context) => {
    const { query } = args
    const { userMoviesService, currentUser } = context
    const currentUserId = get(currentUser, '_id')
    return userMoviesService.search(query, currentUserId)
  },
}
