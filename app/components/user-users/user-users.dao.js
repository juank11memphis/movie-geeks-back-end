import mongodb from 'mongodb'

import UserUsers from './user-users'

export class UserUsersDao {
  /* eslint-disable new-cap */ /* eslint-disable babel/new-cap */
  getByUsers(userId, comparandUserId) {
    return UserUsers.findOne({
      user: userId,
      comparandUser: comparandUserId,
    })
  }

  getTopSimilarUsers(userId, limit) {
    return UserUsers.find({
      $or: [
        { user: mongodb.ObjectID(userId) },
        { comparandUser: mongodb.ObjectID(userId) },
      ],
    })
      .sort({
        score: -1,
      })
      .limit(limit)
  }

  create(data) {
    const newRecord = new UserUsers(data)
    return newRecord.save()
  }

  getUsersWithSameMovie(userId, movieId) {
    return UserUsers.find({
      $or: [
        { user: mongodb.ObjectID(userId) },
        { comparandUser: mongodb.ObjectID(userId) },
      ],
      movies: {
        $elemMatch: { id: mongodb.ObjectID(movieId) },
      },
    })
  }
}

export default new UserUsersDao()
