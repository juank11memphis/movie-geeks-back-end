import { get, keyBy } from 'lodash'

import userUsersDao from './user-users.dao'
import UserUsers from './user-users'
import { updateMany, removeMany } from '../../util/db.util'

export class UserUsersService {
  async getDetailedTopSimilarUsers(userId, limit) {
    const topUserUsers = await this.getTopSimilarUsers(userId, limit)
    const moviesInCommonMap = {}
    const topUsersIds = []
    topUserUsers.forEach(userUsersRecord => {
      if (userUsersRecord.user + '' === userId + '') {
        topUsersIds.push(userUsersRecord.comparandUser + '')
      } else {
        topUsersIds.push(userUsersRecord.user + '')
      }
      userUsersRecord.movies.forEach(
        movie => (moviesInCommonMap[movie.id] = true),
      )
    })
    const moviesInCommonIds = Object.keys(moviesInCommonMap)
    return {
      data: topUserUsers,
      moviesInCommonIds,
      userIds: topUsersIds,
    }
  }

  getTopSimilarUsers(userId, limit) {
    return userUsersDao.getTopSimilarUsers(userId, limit)
  }

  async getByUsers(userId, comparandUserId) {
    const record = await userUsersDao.getByUsers(userId, comparandUserId)
    if (!record) {
      return userUsersDao.getByUsers(comparandUserId, userId)
    }
    return record
  }

  async getByUsersAndMovie(userId, comparandUserId, movieId) {
    const record = await this.getByUsers(userId, comparandUserId)
    if (this.hasMovie(record, movieId)) {
      return record
    }
    return null
  }

  hasMovie(record, movieId) {
    const movies = get(record, 'movies', [])
    const moviesById = keyBy(movies, 'id')
    if (moviesById[movieId]) {
      return true
    }
    return false
  }

  getRatingPoints(rating, comparandRating) {
    let verySimilarLower = rating - 0.5
    verySimilarLower = verySimilarLower < 0 ? 0 : verySimilarLower
    let verySimilarHigher = rating + 0.5
    verySimilarHigher = verySimilarHigher > 10 ? 10 : verySimilarHigher
    if (
      comparandRating >= verySimilarLower &&
      comparandRating <= verySimilarHigher
    ) {
      return 2
    }
    let similarLower = verySimilarLower - 1
    similarLower = similarLower < 0 ? 0 : similarLower
    let similarHigher = verySimilarHigher + 1
    similarHigher = similarHigher > 10 ? 10 : similarHigher
    if (comparandRating >= similarLower && comparandRating <= similarHigher) {
      return 1
    }
    return 0
  }

  getUsersWithSameMovie(userId, movieId) {
    return userUsersDao.getUsersWithSameMovie(userId, movieId)
  }

  async processUsersWithSameMovieNewRating(userMovie, usersWithSameMovie) {
    const bulkUpdates = []
    for (const comparandUserMovie of usersWithSameMovie) {
      const userUsersRecord = await this.getByUsers(
        userMovie.user,
        comparandUserMovie.user,
      )
      const points = this.getRatingPoints(
        userMovie.rating,
        comparandUserMovie.rating,
      )
      if (!userUsersRecord) {
        const data = {
          user: userMovie.user,
          comparandUser: comparandUserMovie.user,
          score: points,
          movies: [{ id: userMovie.movie, points: points }],
        }
        await userUsersDao.create(data)
      } else if (!this.hasMovie(userUsersRecord, userMovie.movie)) {
        const currentMovies = get(userUsersRecord, 'movies', [])
        const updateData = {
          ...userUsersRecord.toObject(),
          score: userUsersRecord.score + points,
          movies: [...currentMovies, { id: userMovie.movie, points: points }],
        }
        bulkUpdates.push({
          id: userUsersRecord._id,
          data: updateData,
        })
      }
    }
    await updateMany(UserUsers, bulkUpdates)
  }

  async processUsersWithSameMovieRatingModified(
    userMovie,
    usersWithSameMovie,
    newRating,
  ) {
    const bulkUpdates = []
    for (const comparandUserMovie of usersWithSameMovie) {
      const oldUserUsersRecord = await this.getByUsers(
        userMovie.user,
        comparandUserMovie.user,
      )
      const oldUserUsersMovie = oldUserUsersRecord.movies.find(
        movie => movie.id + '' === userMovie.movie + '',
      )
      const oldPoints = oldUserUsersMovie.points
      const newPoints = this.getRatingPoints(
        newRating,
        comparandUserMovie.rating,
      )
      const newMovies = oldUserUsersRecord.movies.filter(
        movie => movie.id + '' !== userMovie.movie + '',
      )
      const newUserUsersRecord = {
        ...oldUserUsersRecord.toObject(),
        movies: [...newMovies, { id: userMovie.movie, points: newPoints }],
        score: oldUserUsersRecord.score - oldPoints + newPoints,
      }
      bulkUpdates.push({
        id: oldUserUsersRecord._id,
        data: newUserUsersRecord,
      })
    }
    await updateMany(UserUsers, bulkUpdates)
  }

  async processUsersWithSameMovieRatingDeleted(userMovie, usersWithSameMovie) {
    const bulkUpdates = []
    const bulkDeletes = []
    for (const oldUserUsersRecord of usersWithSameMovie) {
      const oldUserUsersMovie = oldUserUsersRecord.movies.find(
        movie => movie.id + '' === userMovie.movie + '',
      )
      const oldPoints = oldUserUsersMovie.points
      const newMovies = oldUserUsersRecord.movies.filter(
        movie => movie.id + '' !== userMovie.movie + '',
      )
      const newUserUsersRecord = {
        ...oldUserUsersRecord.toObject(),
        movies: [...newMovies],
        score: oldUserUsersRecord.score - oldPoints,
      }
      if (newUserUsersRecord.movies.length > 0) {
        bulkUpdates.push({
          id: oldUserUsersRecord._id,
          data: newUserUsersRecord,
        })
      } else {
        bulkDeletes.push(oldUserUsersRecord._id)
      }
    }
    await updateMany(UserUsers, bulkUpdates)
    await removeMany(UserUsers, bulkDeletes)
  }
}

export default new UserUsersService()
