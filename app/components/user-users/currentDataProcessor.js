import User from '../users/user'
import userUsersService from './user-users.service'
import userMoviesService from '../user-movies/user-movies.service'
import { generalLogger } from '../../util/logs'

const processCurrentData = async () => {
  console.time('processCurrentData')
  generalLogger.info('Current data processor started...')
  const allUsers = await User.find({})
  generalLogger.info(
    `Getting all users from database. Users count: ${allUsers.length}`,
  )
  for (const user of allUsers) {
    const fullCollection = await userMoviesService.getFullCollection(user._id)
    // if (fullCollection.length > 10) {
    //   console.log(
    //     `${user.email} has rated more than 10 movies!!!! Collection length = ${fullCollection.length}`,
    //   )
    // }
    await processUserCollection(fullCollection)
  }
  console.timeEnd('processCurrentData')
}

const processUserCollection = async fullCollection => {
  for (const userMovie of fullCollection) {
    const usersWithSameMovie = await userMoviesService.getUsersWithSameMovie(
      userMovie.user,
      userMovie.movie,
    )
    await userUsersService.processUsersWithSameMovieNewRating(
      userMovie,
      usersWithSameMovie,
    )
  }
}

export { processCurrentData }
