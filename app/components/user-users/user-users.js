import mongoose from 'mongoose'

const Schema = mongoose.Schema

const UserUsersSchema = new Schema(
  {
    user: {
      type: Schema.Types.ObjectId,
      ref: 'User',
    },
    comparandUser: {
      type: Schema.Types.ObjectId,
      ref: 'User',
    },
    score: Number,
    movies: Array,
  },
  {
    timestamps: true,
    collection: 'user-users',
  },
)

export default mongoose.models.UserUsers ||
  mongoose.model('UserUsers', UserUsersSchema)
