import mongodb from 'mongodb'
import mongoose from 'mongoose'

export class DiscoverDao {
  /* eslint-disable new-cap */ /* eslint-disable babel/new-cap */
  discover(userIds, excludeMovieIds) {
    const userIdsForAggr = userIds.map(userId => mongodb.ObjectID(userId))
    const movieIdsForAggr = excludeMovieIds.map(userId =>
      mongodb.ObjectID(userId),
    )
    return mongoose.connection.db
      .collection('user-movies')
      .aggregate([
        {
          $match: {
            user: {
              $in: userIdsForAggr,
            },
            movie: {
              $nin: movieIdsForAggr,
            },
            saved: false,
            rating: { $gt: 6.5 },
          },
        },
        {
          $group: {
            _id: '$movie',
            totalRating: { $sum: '$rating' },
            averageRating: { $avg: '$rating' },
            count: { $sum: 1 },
            users: { $push: '$user' },
          },
        },
        {
          $project: {
            _id: 0,
            movieId: '$_id',
            averageRating: '$averageRating',
            totalRating: '$totalRating',
            count: '$count',
            movie: '$movie',
            users: '$users',
          },
        },
        { $sort: { averageRating: -1 } },
        { $limit: 50 },
      ])
      .toArray()
  }
}

export default new DiscoverDao()
