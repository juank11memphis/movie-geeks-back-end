import userUsersService from '../user-users/user-users.service'
import userMoviesService from '../user-movies/user-movies.service'
import discoverDao from './discover.dao'

export class DiscoverService {
  discover = async userId => {
    const topUserUsers = await userUsersService.getDetailedTopSimilarUsers(
      userId,
      10,
    )

    // we need at least five movies in common to return accurate results
    if (topUserUsers.moviesInCommonIds.length < 5) {
      return { users: [], items: [] }
    }

    const userFullCollection = await userMoviesService.getFullCollection(userId)
    const userFullCollectionMovieIds = userFullCollection.map(
      userMovie => userMovie.movie + '',
    )

    const userFullWatchlist = await userMoviesService.getFullWatchlist(userId)
    const userFullWatchlistMovieIds = userFullWatchlist.map(
      userMovie => userMovie.movie + '',
    )

    const discoverResult = await discoverDao.discover(topUserUsers.userIds, [
      ...userFullCollectionMovieIds,
      ...userFullWatchlistMovieIds,
    ])

    return {
      users: topUserUsers.userIds,
      items: discoverResult,
    }
  }
}

export default new DiscoverService()
