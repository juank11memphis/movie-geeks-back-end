import { get } from 'lodash'
import { AuthenticationError } from 'apollo-server'

export const discoverType = `
  type DiscoverItem {
    id: String!
    movieId: String
    averageRating: Float
    totalRating: Float
    count: Float
    movie: Movie
  }

  type Discover {
    items: [DiscoverItem]
  }
`

const loadDiscoverItemMovie = async (discoverItem, loaders) =>
  loaders.movies.load(discoverItem.movieId)

export const discoverItemTypeResolvers = {
  id: discoverItem => discoverItem.movieId,
  movie: (discoverItem, args, { loaders }) =>
    loadDiscoverItemMovie(discoverItem, loaders),
}

export const discoverQueryTypes = `
  discover: Discover
`

export const discoverQueriesResolvers = {
  discover: (root, args, context) => {
    const { discoverService, currentUser } = context
    if (!currentUser) {
      throw new AuthenticationError('Unauthorized!!')
    }
    const currentUserId = get(currentUser, '_id')
    return discoverService.discover(currentUserId)
  },
}
