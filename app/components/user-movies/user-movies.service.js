import { get, keyBy } from 'lodash'
import { ApolloError } from 'apollo-server'
import { isNil } from 'lodash'

import userMoviesDao from './user-movies.dao'
import moviesService from '../movies/movies.service'
import searchService from '../search/search.service'
import usersService from '../users/users.service'
import * as ratingsUtil from '../../util/ratings.util'
import {
  similarsUsersScoreRatingModifiedQueue,
  similarsUsersScoreNewRatingQueue,
  similarsUsersScoreRatingDeletedQueue,
} from '../../core/jobs'

export class UserMoviesService {
  create(data) {
    return userMoviesDao.create(data)
  }

  getById(userMovieId) {
    return userMoviesDao.getById(userMovieId)
  }

  update(dbUserMovie, data) {
    return userMoviesDao.update(dbUserMovie, data)
  }

  getByUserAndMovie(userId, movieId) {
    return userMoviesDao.getByUserAndMovie(userId, movieId)
  }

  validateUserAndMovie = async (userId, movieId) => {
    const user = await usersService.getDetailById(userId)
    if (!user) {
      throw new ApolloError(`User with id ${userId} does not exists`)
    }
    const movie = await moviesService.getById(movieId)
    if (!movie) {
      throw new ApolloError(`Movie with id ${movieId} does not exists`)
    }
    return { user, movie }
  }

  async getPlayingNow(userId) {
    const movies = await moviesService.getPlayingNow()
    return this.getByUserAndMovies(userId, movies)
  }

  async search(query, userId) {
    const searchResult = await searchService.search(query)
    const userMovies = await this.getByUserAndMovies(
      userId,
      searchResult.movies,
    )
    delete searchResult.movies
    return {
      ...searchResult,
      userMovies: userMovies,
    }
  }

  async getByUserAndMovies(userId, movies) {
    if (!userId) {
      return movies.map(movie => this.groomUserMovieObject(movie))
    }
    const movieIds = movies.map(movie => movie._id)
    const userMovies = await userMoviesDao.getByUserAndMovies(userId, movieIds)
    const userMoviesByMovieId = keyBy(userMovies, 'movie')
    return movies.map(movie =>
      this.groomUserMovieObject(movie, userId, userMoviesByMovieId[movie._id]),
    )
  }

  groomUserMovieObject(movie, userId, userMovie = {}) {
    let theMovie = null
    try {
      theMovie = movie.toObject()
    } catch (error) {
      theMovie = movie
    }
    return {
      user: userId || '',
      movie: movie._id,
      rating: userMovie.rating || null,
      saved: userMovie.saved || false,
      theMovie,
    }
  }

  addComparandUserData(userMovies, comparandUserMovies) {
    const userMoviesMap = keyBy(userMovies, 'movie')
    const finalUserMovies = comparandUserMovies.map(comparandUserMovie => {
      const userMovie = userMoviesMap[comparandUserMovie.movie]
      const theMovie = isNil(userMovie.theMovie.title)
        ? comparandUserMovie.theMovie
        : userMovie.theMovie
      return {
        ...userMovie,
        theMovie,
        comparandUser: {
          userId: comparandUserMovie.user.toString(),
          rating: comparandUserMovie.rating,
          saved: comparandUserMovie.saved,
        },
      }
    })
    return finalUserMovies
  }

  async getComparandUserMovies(userMovies, comparandUserId) {
    const movies = userMovies.map(userMovie => userMovie.movie)
    return this.getByUserAndMovies(comparandUserId, movies)
  }

  loadWatchlist = async (mainUserId, comparandUserId, params = {}) => {
    const { filters, paging } = params
    const result = await userMoviesDao.loadWatchlist(
      mainUserId,
      filters,
      paging,
    )
    const { userMovies: mainUserMovies } = result
    if (!mainUserMovies || mainUserMovies.length <= 0) {
      return { userMovies: [] }
    }
    const comparandUserMovies = await this.getComparandUserMovies(
      mainUserMovies,
      comparandUserId,
    )
    const finalUserMovies = this.addComparandUserData(
      mainUserMovies,
      comparandUserMovies,
    )
    return { ...result, userMovies: finalUserMovies }
  }

  async loadWatchlistAs(mainUserId, impersonateUserId, params = {}) {
    const { filters, paging } = params
    const result = await userMoviesDao.loadWatchlist(
      impersonateUserId,
      filters,
      paging,
    )
    const { userMovies: impersonateUserMovies } = result
    if (!impersonateUserMovies || impersonateUserMovies.length <= 0) {
      return { userMovies: [] }
    }
    const mainUserMovies = await this.getComparandUserMovies(
      impersonateUserMovies,
      mainUserId,
    )
    const finalUserMovies = this.addComparandUserData(
      mainUserMovies,
      impersonateUserMovies,
    )
    return { ...result, userMovies: finalUserMovies }
  }

  loadCollection = async (mainUserId, comparandUserId, params = {}) => {
    const { filters, paging } = params
    const result = await userMoviesDao.loadCollection(
      mainUserId,
      filters,
      paging,
    )
    const { userMovies: mainUserMovies } = result
    if (!mainUserMovies || mainUserMovies.length <= 0) {
      return { userMovies: [] }
    }
    const comparandUserMovies = await this.getComparandUserMovies(
      mainUserMovies,
      comparandUserId,
    )
    const finalUserMovies = this.addComparandUserData(
      mainUserMovies,
      comparandUserMovies,
    )
    return { ...result, userMovies: finalUserMovies }
  }

  async loadCollectionAs(mainUserId, impersonateUserId, params = {}) {
    const { filters, paging } = params
    const result = await userMoviesDao.loadCollection(
      impersonateUserId,
      filters,
      paging,
    )
    const { userMovies: impersonateUserMovies } = result
    if (!impersonateUserMovies || impersonateUserMovies.length <= 0) {
      return { userMovies: [] }
    }
    const mainUserMovies = await this.getComparandUserMovies(
      impersonateUserMovies,
      mainUserId,
    )
    const finalUserMovies = this.addComparandUserData(
      mainUserMovies,
      impersonateUserMovies,
    )
    return { ...result, userMovies: finalUserMovies }
  }

  addToWatchlist = async (userId, movieId) => {
    await this.validateUserAndMovie(userId, movieId)
    const dbUserMovie = await this.getByUserAndMovie(userId, movieId)
    if (!dbUserMovie) {
      return this.create({
        user: userId,
        movie: movieId,
        saved: true,
      })
    } else {
      return this.update(dbUserMovie, { saved: true })
    }
  }

  removeFromWatchlist = async (userId, movieId) => {
    const dbUserMovie = await this.getByUserAndMovie(userId, movieId)
    if (!dbUserMovie) {
      throw new ApolloError('Invalid user or movie')
    }
    return this.update(dbUserMovie, { saved: false })
  }

  rateMovie = async (userId, movieId, rating) => {
    await this.validateUserAndMovie(userId, movieId)

    const currentUserMovie = await this.getByUserAndMovie(userId, movieId)
    const oldRating = currentUserMovie ? currentUserMovie.rating : null
    const newRating = ratingsUtil.getValidRatingValue(rating)
    const userMovieData = { rating: newRating, saved: false }

    await this.createOrUpdate(currentUserMovie, userId, movieId, userMovieData)
    await this.recalculateMovieRatingsData(movieId, oldRating, newRating)

    this.executePostRatingProcesses(userId, movieId, oldRating, newRating)

    return this.getByUserAndMovie(userId, movieId)
  }

  async recalculateMovieRatingsData(movieId, oldRating, newRating) {
    let movie = await moviesService.getById(movieId)
    const ratingsData = ratingsUtil.recalculateRatingsData(
      movie,
      oldRating,
      newRating,
    )
    movie = await moviesService.update(movie, { ratingsData })
  }

  async executePostRatingProcesses(userId, movieId, oldRating, newRating) {
    if (oldRating) {
      similarsUsersScoreRatingModifiedQueue.add({ userId, movieId, newRating })
    }
    similarsUsersScoreNewRatingQueue.add({ userId, movieId })
  }

  removeMovieRating = async (userId, movieId) => {
    const { movie: dbMovie } = await this.validateUserAndMovie(userId, movieId)
    const dbUserMovie = await this.getByUserAndMovie(userId, dbMovie._id)
    const currentRating = get(dbUserMovie, 'rating', null)
    if (!dbUserMovie || !currentRating) {
      return dbUserMovie
    }
    const ratingsData = ratingsUtil.recalculateRatingsDataOnRemoveRating(
      dbMovie,
      currentRating,
    )
    await this.update(dbUserMovie, { saved: false, rating: null })
    await moviesService.update(dbMovie, { ratingsData })
    similarsUsersScoreRatingDeletedQueue.add({ userId, movieId })
    return this.getByUserAndMovie(userId, movieId)
  }

  createOrUpdate = async (currentUserMovie, userId, movieId, data) => {
    if (currentUserMovie === null) {
      return userMoviesDao.create({
        user: userId,
        movie: movieId,
        ...data,
      })
    }
    return userMoviesDao.update(currentUserMovie, { ...data })
  }

  getUsersWithSameMovie(userId, movieId) {
    return userMoviesDao.getUsersWithSameMovie(userId, movieId)
  }

  getFullCollection(userId) {
    return userMoviesDao.getFullCollection(userId)
  }

  getFullWatchlist(userId) {
    return userMoviesDao.getFullWatchlist(userId)
  }
}

export default new UserMoviesService()
