import { AuthenticationError } from 'apollo-server'

export const userMovieType = `
  type UserMoviesResponse {
    userMovies: [UserMovie]
    pagingMetadata: PagingMetadata
  }
  type UserMovieComparand {
    userId: String
    rating: Float
    saved: Boolean
  }
  type UserMovie {
    id: String!
    userId: String
    movieId: String
    rating: Float
    saved: Boolean
    movie: Movie
    comparandUser: UserMovieComparand
  }
`

export const userMovieTypeResolver = {
  id: userMovie => `${userMovie.user.toString()}-${userMovie.movie.toString()}`,
  userId: userMovie => {
    if (userMovie.user) {
      return userMovie.user.toString()
    }
    return null
  },
  movieId: userMovie => userMovie.movie.toString(),
  movie: userMovie => ({
    ...userMovie.theMovie,
    id: userMovie.theMovie._id.toString(),
  }),
  comparandUser: ({ comparandUser = {} }) =>
    comparandUser.userId ? comparandUser : null,
}

export const userMoviesQueryTypes = `
  loadWatchlist(mainUserId:String, comparandUserId: String, params: MovieParams): UserMoviesResponse
  loadWatchlistAs(mainUserId:String, impersonateUserId: String, params: MovieParams): UserMoviesResponse
  loadCollection(mainUserId:String, comparandUserId: String, params: MovieParams): UserMoviesResponse
  loadCollectionAs(mainUserId: String, impersonateUserId: String, params: MovieParams): UserMoviesResponse
`

export const userMoviesQueryResolvers = {
  loadWatchlist: (root, args, context) => {
    const { mainUserId, comparandUserId, params } = args
    const { userMoviesService } = context
    return userMoviesService.loadWatchlist(mainUserId, comparandUserId, params)
  },
  loadWatchlistAs: (root, args, context) => {
    const { mainUserId, impersonateUserId, params } = args
    const { userMoviesService } = context
    return userMoviesService.loadWatchlistAs(
      mainUserId,
      impersonateUserId,
      params,
    )
  },
  loadCollection: (root, args, context) => {
    const { mainUserId, comparandUserId, params } = args
    const { userMoviesService } = context
    return userMoviesService.loadCollection(mainUserId, comparandUserId, params)
  },
  loadCollectionAs: (root, args, context) => {
    const { mainUserId, impersonateUserId, params } = args
    const { userMoviesService } = context
    return userMoviesService.loadCollectionAs(
      mainUserId,
      impersonateUserId,
      params,
    )
  },
}

export const userMoviesMutationTypes = `
  addToWatchlist(movieId: String): UserMovie
  removeFromWatchlist(movieId: String): UserMovie
  rateMovie(movieId: String, rating: Float): UserMovie
  removeMovieRating(movieId: String): UserMovie
`

export const userMoviesMutationsResolvers = {
  addToWatchlist: (root, args, context) => {
    const { movieId } = args
    const { userMoviesService, currentUser } = context
    if (!currentUser) {
      throw new AuthenticationError('Unauthorized!!')
    }
    return userMoviesService.addToWatchlist(currentUser._id, movieId)
  },
  removeFromWatchlist: (root, args, context) => {
    const { movieId } = args
    const { userMoviesService, currentUser } = context
    if (!currentUser) {
      throw new AuthenticationError('Unauthorized!!')
    }
    return userMoviesService.removeFromWatchlist(currentUser._id, movieId)
  },
  rateMovie: (root, args, context) => {
    const { movieId, rating } = args
    const { userMoviesService, currentUser } = context
    if (!currentUser) {
      throw new AuthenticationError('Unauthorized!!')
    }
    return userMoviesService.rateMovie(currentUser._id, movieId, rating)
  },
  removeMovieRating: (root, args, context) => {
    const { movieId } = args
    const { userMoviesService, currentUser } = context
    if (!currentUser) {
      throw new AuthenticationError('Unauthorized!!')
    }
    return userMoviesService.removeMovieRating(currentUser._id, movieId)
  },
}
