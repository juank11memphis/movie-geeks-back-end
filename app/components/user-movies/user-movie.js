import mongoose from 'mongoose'

const Schema = mongoose.Schema

const UserMovieSchema = new Schema(
  {
    user: {
      type: Schema.Types.ObjectId,
      ref: 'User',
    },
    movie: {
      type: Schema.Types.ObjectId,
      ref: 'Movie',
    },
    rating: Number,
    saved: {
      type: Boolean,
      default: false,
    },
  },
  {
    timestamps: true,
    collection: 'user-movies',
  },
)

export default mongoose.models.UserMovie ||
  mongoose.model('UserMovie', UserMovieSchema)
