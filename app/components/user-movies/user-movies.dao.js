import mongodb from 'mongodb'
import MongoPaging from 'mongo-cursor-pagination'
import { get } from 'lodash'

import UserMovie from './user-movie'
import { constructUserMovieAggregate } from '../../util/movies.util'

export class UserMoviesDao {
  /* eslint-disable new-cap */ /* eslint-disable babel/new-cap */
  create(data) {
    const newUserMovie = new UserMovie(data)
    return newUserMovie.save()
  }

  getById(userMovieId) {
    return UserMovie.findById(userMovieId)
  }

  async update(dbUserMovie, data) {
    await dbUserMovie.updateOne(data)
    const dbUserMovieUpdated = await this.getById(dbUserMovie._id)
    return dbUserMovieUpdated
  }

  getByUserAndMovie(userId, movieId) {
    return UserMovie.findOne({ user: userId, movie: movieId })
  }

  async loadWatchlist(userId, filters, paging) {
    const nextParam = get(paging, 'next')
    const limitParam = get(paging, 'limit')
    const aggregationQuery = constructUserMovieAggregate({
      baseMatchQuery: { user: mongodb.ObjectID(userId), saved: true },
      filters,
    })
    const {
      results,
      previous,
      hasPrevious,
      next,
      hasNext,
    } = await MongoPaging.aggregate(UserMovie.collection, {
      aggregation: aggregationQuery,
      limit: limitParam,
      next: nextParam,
      paginatedField: 'updatedAt',
    })
    return {
      userMovies: results,
      pagingMetadata: {
        previous,
        hasPrevious,
        next,
        hasNext,
      },
    }
  }

  async loadCollection(userId, filters, paging) {
    const nextParam = get(paging, 'next')
    const limitParam = get(paging, 'limit')
    const aggregationQuery = constructUserMovieAggregate({
      baseMatchQuery: {
        user: mongodb.ObjectID(userId),
        saved: false,
        rating: { $gt: -1 },
      },
      filters,
    })
    const {
      results,
      previous,
      hasPrevious,
      next,
      hasNext,
    } = await MongoPaging.aggregate(UserMovie.collection, {
      aggregation: aggregationQuery,
      limit: limitParam,
      next: nextParam,
      paginatedField: 'rating',
    })

    return {
      userMovies: results,
      pagingMetadata: {
        previous,
        hasPrevious,
        next,
        hasNext,
      },
    }
  }

  getByUserAndMovies(userId, movieIds) {
    return UserMovie.find({ user: userId, movie: { $in: movieIds } })
  }

  getUsersWithSameMovie(userId, movieId) {
    return UserMovie.find({
      user: { $not: { $eq: mongodb.ObjectID(userId) } },
      movie: mongodb.ObjectID(movieId),
      saved: false,
      rating: { $gt: -1 },
    })
  }

  getFullCollection(userId) {
    return UserMovie.find({
      user: mongodb.ObjectID(userId),
      saved: false,
      rating: { $gt: -1 },
    })
  }

  getFullWatchlist(userId) {
    return UserMovie.find({
      user: mongodb.ObjectID(userId),
      saved: true,
    })
  }
}

export default new UserMoviesDao()
