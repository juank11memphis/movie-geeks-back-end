import { difference, keyBy, map } from 'lodash'

import themoviedbApi from '../../api/themoviedb/themoviedb.api'
import moviesDao from './movies.dao'
import { isValidDBId } from '../../util/db.util'

export class MoviesService {
  async loadByIds(ids) {
    const movies = await moviesDao.loadByIds(ids)
    const moviesById = keyBy(movies, '_id')
    return ids.map(movieId => moviesById[movieId])
  }

  create(data) {
    return moviesDao.create(data)
  }

  update(dbMovie, data) {
    return moviesDao.update(dbMovie, data)
  }

  getPlayingNow = async () => {
    const tmdbMovies = await themoviedbApi.getPlayingNow()
    const dbMovies = await this.saveAllTMDBMovies(tmdbMovies || [])
    return dbMovies
  }

  saveAllTMDBMovies = async (tmdbMovies = []) => {
    console.time('moviesService.saveAllTMDBMovies')
    const tmdbMoviesMap = keyBy(tmdbMovies, 'id')
    const tmdbIds = tmdbMovies.map(tmdbMovie => tmdbMovie.id + '')
    const alreadyExistingMovies = await moviesDao.findByTmdbIds(tmdbIds)
    const alreadyExistingMoviesMap = keyBy(alreadyExistingMovies, 'tmdbId')
    const alreadyExistingTmdbIds = map(alreadyExistingMovies, 'tmdbId')
    const newTmdbIds = difference(tmdbIds, alreadyExistingTmdbIds)
    const dbMovies = []
    for (const tmdbId of tmdbIds) {
      const isNew = newTmdbIds.indexOf(tmdbId) > -1
      if (isNew) {
        const newMovie = tmdbMoviesMap[tmdbId]
        const { id, ...rest } = newMovie
        const dbMovie = await this.create({ tmdbId: id, ...rest })
        dbMovies.push(dbMovie)
      } else {
        dbMovies.push(alreadyExistingMoviesMap[tmdbId])
      }
    }
    console.timeEnd('moviesService.saveAllTMDBMovies')
    return dbMovies
  }

  getById(id) {
    return moviesDao.getById(id)
  }

  getByTmdbId(tmdbId) {
    return moviesDao.getByTmdbId(tmdbId + '')
  }

  getByIdOrTmdbId(id) {
    if (isValidDBId(id)) {
      return this.getById(id)
    }
    return this.getByTmdbId(id)
  }

  async getYoutubeOfficialTrailerKey(id) {
    const dbMovie = await this.getById(id)
    if (dbMovie.youtubeTrailerKey) {
      return dbMovie.youtubeTrailerKey
    }
    const key = await await themoviedbApi.getOfficialTrailerYoutubeKey(
      dbMovie.tmdbId,
    )
    await this.update(dbMovie, { youtubeTrailerKey: key })
    return key
  }
}

export default new MoviesService()
