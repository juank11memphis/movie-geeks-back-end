import mongoose from 'mongoose'

const Schema = mongoose.Schema

const MovieSchema = new Schema(
  {
    title: String,
    originalTitle: String,
    posterPath: String,
    genres: Array,
    overview: String,
    releaseDate: String,
    year: String,
    tmdbId: String,
    ratingsData: {}, // { globalRating, ratingsCount, ratingsSum }
    youtubeTrailerKey: String,
  },
  {
    timestamps: true,
    collection: 'movies',
  },
)

export default mongoose.models.Movie || mongoose.model('Movie', MovieSchema)
