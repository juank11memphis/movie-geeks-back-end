import Movie from './movie'

export class MoviesDao {
  loadByIds(ids) {
    return Movie.find({ _id: { $in: ids } })
  }

  create(data) {
    const newMovie = new Movie(data)
    return newMovie.save()
  }

  async update(dbMovie, data) {
    await dbMovie.updateOne(data)
    const dbMovieUpdated = await this.getById(dbMovie._id)
    return dbMovieUpdated
  }

  getById(id) {
    return Movie.findById(id)
  }

  getByTmdbId(tmdbId) {
    return Movie.findOne({ tmdbId: tmdbId })
  }

  findByTmdbIds(ids) {
    return Movie.find({
      tmdbId: {
        $in: ids,
      },
    })
  }
}

export default new MoviesDao()
