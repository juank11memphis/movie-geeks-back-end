import { get } from 'lodash'

export const movieType = `
  type Movie {
    id: String!
    title: String
    originalTitle: String
    posterPath: String
    genres: [String]
    overview: String
    releaseDate: String
    year: String
    youtubeTrailerKey: String
  }
  input MovieFilters {
    genres: [String]
    minimumRating: Float
    yearsRange: [Float]
  }
  input MovieParams {
    filters: MovieFilters
    paging: PagingParams
  }
`

export const moviesQueryTypes = `
  getPlayingNow: [UserMovie]
  getYoutubeOfficialTrailerKey(movieId: String): String
`

export const moviesQueriesResolvers = {
  getPlayingNow: (root, args, context) => {
    const { userMoviesService, currentUser } = context
    const currentUserId = get(currentUser, '_id')
    return userMoviesService.getPlayingNow(currentUserId)
  },
  getYoutubeOfficialTrailerKey: (root, args, context) => {
    const { moviesService } = context
    const { movieId } = args
    return moviesService.getYoutubeOfficialTrailerKey(movieId)
  },
}
