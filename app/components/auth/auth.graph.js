import { AuthenticationError } from 'apollo-server'

export const authType = `
  type AuthResponse {
    user: User!
    token: String!
  }
`

export const authMutationTypes = `
  signin(email: String, password: String): AuthResponse
  signup(user: UserInput): AuthResponse
  authSocial(user: UserInput): AuthResponse
  requestPasswordReset(email: String): User
  resetPassword(password: String): AuthResponse
  refreshToken: AuthResponse
`

export const authMutationsResolvers = {
  signin: async (root, args, context) => {
    const { email, password } = args
    const { authService } = context
    return authService.signin(email, password)
  },
  signup: async (root, args, context) => {
    const { user } = args
    const { authService } = context
    return authService.signup(user)
  },
  authSocial: (root, args, context) => {
    const { user } = args
    const { authService } = context
    return authService.authSocial(user)
  },
  requestPasswordReset: (root, args, context) => {
    const { email } = args
    const { authService } = context
    return authService.requestPasswordReset(email)
  },
  resetPassword: (root, args, context) => {
    const { password } = args
    const { authService, resetPasswordUser } = context
    if (!resetPasswordUser) {
      throw new AuthenticationError('Unauthorized!!')
    }
    return authService.resetPassword(resetPasswordUser._id, password)
  },
  refreshToken: (root, args, context) => {
    const { currentUser, tokenData, authService } = context
    if (!currentUser) {
      throw new AuthenticationError('Unauthorized!!')
    }
    return authService.refreshToken(tokenData)
  },
}
