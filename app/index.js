import { errorsLogger } from './util/logs'
import configUtil from './util/config.util'

// Initializes configuration
configUtil
  .init()
  .then(() => start())
  .catch(err =>
    errorsLogger.error('Unexpected error loading configuration', err),
  )

const start = () => {
  try {
    // Initialize Database
    require('./core/database')
  } catch (error) {
    errorsLogger.error('Unexpected error initializing database...', error)
  }
  try {
    // Initialize Jobs
    require('./core/jobs')
  } catch (error) {
    errorsLogger.error('Unexpected error initializing jobs...', error)
  }
  try {
    // Initialize Server
    require('./core/server')
  } catch (error) {
    errorsLogger.error('Unexpected error initializing server...', error)
  }
}
