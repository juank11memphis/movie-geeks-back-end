import { get } from 'lodash'

const baseUserMovieStages = [
  {
    $lookup: {
      from: 'movies',
      localField: 'movie',
      foreignField: '_id',
      as: 'theMovie',
    },
  },
  { $unwind: '$theMovie' },
  { $addFields: { yearInt: { $toInt: '$theMovie.year' } } },
]

export const constructUserMovieAggregate = ({ baseMatchQuery, filters }) => {
  return [...baseUserMovieStages, getMatchStage(baseMatchQuery, filters)]
}

const getMatchStage = (baseMatchQuery, filters) => ({
  $match: {
    ...baseMatchQuery,
    ...getGenreFilters(filters),
    ...getYearsRangeFilters(filters),
    ...getMinimumRatingFilter(filters),
  },
})

const getGenreFilters = filters => {
  const genresFilters = get(filters, 'genres')
  return genresFilters
    ? {
        'theMovie.genres': { $in: genresFilters },
      }
    : {}
}

const getYearsRangeFilters = filters => {
  const yearsRangeFilters = get(filters, 'yearsRange')
  return yearsRangeFilters
    ? {
        yearInt: {
          $gte: parseInt(yearsRangeFilters[0]),
          $lte: parseInt(yearsRangeFilters[1]),
        },
      }
    : {}
}

const getMinimumRatingFilter = filters => {
  const minimumRatingFilter = get(filters, 'minimumRating')
  return minimumRatingFilter
    ? {
        rating: { $gte: parseInt(minimumRatingFilter) },
      }
    : {}
}
