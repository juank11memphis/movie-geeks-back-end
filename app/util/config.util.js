import parametersStore from '../api/aws/aws-ssm.api'
import { generalLogger } from './logs'

export class ConfigUtil {
  defaults = {
    environment: 'local',
    test: {
      mongodb_url: 'mongodb://mongodb:27017/test?authSource=admin',
      mongo_username: 'juanca',
      mongo_password: 'password',
      redis_url: 'redis://redis:6379',
      port: 5678,
      token_secret: 'a_test_secret',
      reset_password_token_secret: 'another_test_secret',
      token_expiration: '1d',
      reset_password_token_expiration: '1d',
      security_salt_rounds: 4,
      themoviedb_key: 'a_movide_db_key',
      mailgun_apikey: 'an_awesome_key',
      mailgun_domain: 'awesome@domain.com',
      support_email: 'support@moviegeeks.co',
      reset_password_email_link: 'http://localhost:3000/reset-password',
    },
  }
  values = {}

  init = async () => {
    generalLogger.info(
      `loading configuration for env: ${this.getEnvironment()}`,
    )
    this.loadConfigValuesFromEnvFile()
    if (this.isTest() || this.isLocal()) {
      generalLogger.info('configuration loaded from env file...')
      return this.values
    }
    const environment = this.getEnvironment()
    const path = `/${environment}/moviegeeks/`
    this.values = await parametersStore.getParametersByPath(path)
    this.values = this.parseValues(this.values)
    generalLogger.info('configuration loaded from aws parameters store...')
    return this.values
  }

  loadConfigValuesFromEnvFile() {
    require('dotenv').config()
  }

  // Takes the parameters returned by AWS and creates an easy to use dictionary
  parseValues(values) {
    const environment = this.getEnvironment()
    const path = `/${environment}/moviegeeks/`
    const map = {}
    values.forEach(param => {
      const paramKey = param['Name'].replace(path, '')
      map[paramKey] = param['Value']
    })
    return map
  }

  getVersion() {
    return require('../../package.json').version
  }

  getEnvironment() {
    if (!process.env.NODE_ENV) {
      return this.defaults.environment
    }
    return process.env.NODE_ENV
  }

  getValue(key) {
    if (this.isTest() && this.defaults.test[key]) {
      return this.defaults.test[key]
    }
    if (process.env[key]) {
      return process.env[key]
    }
    return this.values[key]
  }

  getValueAsInt(key) {
    const valueStr = this.getValue(key)
    return parseInt(valueStr)
  }

  isTest() {
    const environment = this.getEnvironment()
    return environment.indexOf('test') > -1
  }

  isLocal() {
    const environment = this.getEnvironment()
    return environment === 'local'
  }

  isProd() {
    const environment = this.getEnvironment()
    return environment === 'production'
  }
}

export default new ConfigUtil()
