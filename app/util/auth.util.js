import jwt from 'jsonwebtoken'
import bcrypt from 'bcryptjs'
import { AuthenticationError } from 'apollo-server'

import configUtil from './config.util'

export const generateToken = user => {
  return jwt.sign(
    { id: user._id, roles: user.roles },
    configUtil.getValue('token_secret'),
    {
      expiresIn: configUtil.getValue('token_expiration'),
    },
  )
}

export const generateResetPasswordToken = user => {
  return jwt.sign(
    { id: user._id },
    configUtil.getValue('reset_password_token_secret'),
    {
      expiresIn: configUtil.getValue('reset_password_token_expiration'),
    },
  )
}

export const validateToken = async (
  authorization = '',
  tokenSecretKey = 'token_secret',
) => {
  const [jwtType, token] = authorization.split(' ')
  if (jwtType.toLowerCase() !== 'bearer' || !token) {
    throw new AuthenticationError('Unauthorized!!')
  }
  let decoded
  try {
    decoded = await jwt.verify(token, configUtil.getValue(tokenSecretKey))
  } catch (error) {
    throw new AuthenticationError('Unauthorized!!')
  }
  return {
    userId: decoded.id,
    userRoles: decoded.roles,
  }
}

export const getTokenData = async (token, tokenSecretKey = 'token_secret') => {
  let tokenData
  try {
    tokenData = await validateToken(token, tokenSecretKey)
  } catch (error) {
    tokenData = null
  }
  return tokenData
}

export const validatePermissions = (roles, permissionToFind) => {
  if (!roles || roles.length === 0) {
    throw new AuthenticationError('Not enough permissions')
  }
  let permissionFound = false
  roles.forEach(({ permissions }) => {
    if (!permissions) {
      return
    }
    permissions.forEach(permission => {
      if (permission === permissionToFind) {
        permissionFound = true
      }
    })
  })
  if (!permissionFound) {
    throw new AuthenticationError('Not enough permissions')
  }
  return true
}

export const comparePasswords = (password, encryptedPassword) => {
  return bcrypt.compareSync(password, encryptedPassword)
}

export const hashPassword = password => {
  return bcrypt.hashSync(
    password,
    configUtil.getValueAsInt('security_salt_rounds'),
  )
}
