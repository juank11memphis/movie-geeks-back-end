export const isValidDBId = id => {
  const stringId = id + ''
  return stringId.match(/^[0-9a-fA-F]{24}$/)
}

export const getBulkUpdateObject = (id, data) => ({
  updateOne: {
    filter: { _id: id },
    update: { $set: { ...data } },
  },
})

export const updateMany = async (modelClass, updates = []) => {
  if (updates.length === 0) {
    return []
  }
  const bulkUpdates = []
  updates.forEach(({ id, data }) => {
    bulkUpdates.push(getBulkUpdateObject(id, data))
  })
  await modelClass.collection.bulkWrite(bulkUpdates, { ordered: true, w: 1 })
}

export const removeMany = (modelClass, deletes = []) => {
  if (deletes.length === 0) {
    return []
  }
  return modelClass.remove({ _id: { $in: deletes } })
}
