import generalLogger from './general.logger'
import errorsLogger from './errors.logger'

export { generalLogger, errorsLogger }
