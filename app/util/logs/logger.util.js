import bunyan from 'bunyan'
import consoleStream from 'bunyan-console-stream'

import configUtil from '../config.util'

const streamOptions = {
  stderrThreshold: 40, // log warning, error and fatal messages on STDERR
}

const logger = bunyan.createLogger({
  name: 'node-starter-kit',
  streams: [
    {
      path: './app-logs.log',
    },
    {
      type: 'raw',
      stream: consoleStream.createStream(streamOptions),
    },
  ],
})

export default logger
