import logger from './logger.util'

const generalLogger = logger.child({ type: 'general' })

export default generalLogger
