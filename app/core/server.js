import app from './app'
import graphServer from '../graphql/graphql.server'
import configUtil from '../util/config.util'
import { generalLogger, errorsLogger } from '../util/logs'
import { processCurrentData } from '../components/user-users/currentDataProcessor'

graphServer.applyMiddleware({ app })

const port = configUtil.getValue('port')
const environment = configUtil.getEnvironment()
const version = configUtil.getVersion()

const server = app.listen(port, () => {
  // eslint-disable-next-line no-console
  generalLogger.info(`
    Server started successfully
    Port: ${port}
    Env: ${environment}
    Version: ${version}
  `)
  if (environment === 'local') {
    try {
      processCurrentData()
    } catch (error) {
      errorsLogger.error('Unexpected error processing current data...', error)
    }
  }
})

export default server
