import mongoose from 'mongoose'

import configUtil from '../util/config.util'
import { generalLogger } from '../util/logs'

// Use native promises
mongoose.Promise = global.Promise

const mongoUrl = configUtil.getValue('mongodb_url')
const mongoUsername = configUtil.getValue('mongo_username')
const mongoPassword = configUtil.getValue('mongo_password')

// Connect to our mongo database;
mongoose.connect(mongoUrl, {
  socketTimeoutMS: 10000,
  useNewUrlParser: true,
  user: mongoUsername,
  pass: mongoPassword,
})
mongoose.connection
  // eslint-disable-next-line no-console
  .once('open', () => generalLogger.info('Successfully connected to MongDB!'))
  .on('error', err => {
    throw err
  })
