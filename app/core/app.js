import { Router } from 'express'
import express from 'express'
import cors from 'cors'
import bodyParser from 'body-parser'
import methodOverride from 'method-override'
import helmet from 'helmet'
import compress from 'compression'

import configUtil from '../util/config.util'

const app = express()

const routes = new Router()
routes.get('/api', (req, res, next) =>
  res.status(200).json(`{"api_version": "${configUtil.getVersion()}"}`),
)

app.use(helmet())
app.use(cors())

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.use(compress())

app.use(methodOverride())

app.use('/', routes)

export default app
