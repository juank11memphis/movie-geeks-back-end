import Queue from 'bull'

import configUtil from '../util/config.util'
import userUsersService from '../components/user-users/user-users.service'
import userMoviesService from '../components/user-movies/user-movies.service'

const redisUrl = configUtil.getValue('redis_url')

const similarsUsersScoreNewRatingQueue = new Queue(
  'similars users score new rating',
  redisUrl,
)
similarsUsersScoreNewRatingQueue.process(async job => {
  const { userId, movieId } = job.data
  const userMovie = await userMoviesService.getByUserAndMovie(userId, movieId)
  const usersWithSameMovie = await userMoviesService.getUsersWithSameMovie(
    userId,
    movieId,
  )
  await userUsersService.processUsersWithSameMovieNewRating(
    userMovie,
    usersWithSameMovie,
  )
  return true
})

const similarsUsersScoreRatingModifiedQueue = new Queue(
  'similars users score update',
  redisUrl,
)
similarsUsersScoreRatingModifiedQueue.process(async job => {
  const { userId, movieId, newRating } = job.data
  const userMovie = await userMoviesService.getByUserAndMovie(userId, movieId)
  const usersWithSameMovie = await userMoviesService.getUsersWithSameMovie(
    userId,
    movieId,
  )
  await userUsersService.processUsersWithSameMovieRatingModified(
    userMovie,
    usersWithSameMovie,
    newRating,
  )
  return true
})

const similarsUsersScoreRatingDeletedQueue = new Queue(
  'similars users score decrease',
  redisUrl,
)
similarsUsersScoreRatingDeletedQueue.process(async job => {
  const { userId, movieId } = job.data
  const userMovie = await userMoviesService.getByUserAndMovie(userId, movieId)
  const usersWithSameMovie = await userUsersService.getUsersWithSameMovie(
    userId,
    movieId,
  )
  await userUsersService.processUsersWithSameMovieRatingDeleted(
    userMovie,
    usersWithSameMovie,
  )
  return true
})

export {
  similarsUsersScoreNewRatingQueue,
  similarsUsersScoreRatingModifiedQueue,
  similarsUsersScoreRatingDeletedQueue,
}
