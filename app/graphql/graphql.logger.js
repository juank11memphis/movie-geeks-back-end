import { generalLogger, errorsLogger } from '../util/logs'

export default class GraphQLLogger {
  executionDidStart({ executionArgs = {} }) {
    const { operationName } = executionArgs
    generalLogger.info(`Handling graph request: ${operationName}`)
  }
  willSendResponse({ graphqlResponse }) {
    const { errors = [] } = graphqlResponse
    if (!errors || errors.length <= 0) {
      return
    }
    const error = errors[0]
    const { extensions = {}, message } = error
    const { code = 'INTERNAL_SERVER_ERROR' } = extensions
    if (code === 'UNAUTHENTICATED') {
      errorsLogger.error(message)
    } else {
      errorsLogger.error(error.stack)
    }
  }
}
