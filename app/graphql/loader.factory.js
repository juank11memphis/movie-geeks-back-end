import DataLoader from 'dataloader'

export const createDataLoader = serviceInstance => {
  const findItems = ids => serviceInstance['loadByIds'](ids)
  return new DataLoader(findItems)
}
