import { makeExecutableSchema } from 'graphql-tools'

import typeDefs from './types'
import resolvers from './resolvers'

const getSchema = () =>
  makeExecutableSchema({
    typeDefs,
    resolvers,
  })

const schema = getSchema()

export default schema
