import { authMutationsResolvers } from '../components/auth/auth.graph'
import {
  userQueriesResolvers,
  userMutationsResolvers,
  userTypeResolver,
} from '../components/users/users.graph'
import { searchQueriesResolvers } from '../components/search/search.graph'
import { moviesQueriesResolvers } from '../components/movies/movies.graph'
import {
  userMovieTypeResolver,
  userMoviesQueryResolvers,
  userMoviesMutationsResolvers,
} from '../components/user-movies/user-movies.graph'
import {
  discoverQueriesResolvers,
  discoverItemTypeResolvers,
} from '../components/discover/discover.graph'

export default {
  User: userTypeResolver,
  UserMovie: userMovieTypeResolver,
  DiscoverItem: discoverItemTypeResolvers,
  Query: {
    ...userQueriesResolvers,
    ...searchQueriesResolvers,
    ...moviesQueriesResolvers,
    ...userMoviesQueryResolvers,
    ...discoverQueriesResolvers,
  },
  Mutation: {
    ...userMutationsResolvers,
    ...authMutationsResolvers,
    ...userMoviesMutationsResolvers,
  },
}
