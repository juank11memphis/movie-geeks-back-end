import { ApolloServer } from 'apollo-server-express'

import schema from './schema'
import context from './context'
import { extractMessageFromError } from '../util/errors.util'
import GraphQLLogger from './graphql.logger'

const graphServer = new ApolloServer({
  schema,
  context,
  formatError: error => ({ message: extractMessageFromError(error) }),
  extensions: [() => new GraphQLLogger()],
})

export default graphServer
