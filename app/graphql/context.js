import usersService from '../components/users/users.service'
import authService from '../components/auth/auth.service'
import userRolesService from '../components/user-roles/user-roles.service'
import searchService from '../components/search/search.service'
import moviesService from '../components/movies/movies.service'
import userMoviesService from '../components/user-movies/user-movies.service'
import discoverService from '../components/discover/discover.service'
import { createDataLoader } from '../graphql/loader.factory'
import { getTokenData } from '../util/auth.util'

export default async function({ req, res }) {
  let user = null
  let resetPasswordUser = null
  const token = req.headers['authorization']
  const resetPasswordToken = req.headers['reset-password-token']
  const tokenData = await getTokenData(token)
  const resetPasswordTokenData = await getTokenData(
    resetPasswordToken,
    'reset_password_token_secret',
  )
  if (tokenData) {
    user = await usersService.getDetailById(tokenData.userId)
  }
  if (resetPasswordTokenData) {
    resetPasswordUser = await usersService.getDetailById(
      resetPasswordTokenData.userId,
    )
  }
  return {
    token: req.headers['authorization'],
    currentUser: user,
    resetPasswordUser,
    tokenData,
    usersService,
    authService,
    searchService,
    moviesService,
    userMoviesService,
    discoverService,
    loaders: {
      users: createDataLoader(usersService),
      userRoles: createDataLoader(userRolesService),
      movies: createDataLoader(moviesService),
    },
  }
}
