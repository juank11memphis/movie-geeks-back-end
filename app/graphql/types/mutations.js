import { authMutationTypes } from '../../components/auth/auth.graph'
import { userMoviesMutationTypes } from '../../components/user-movies/user-movies.graph'

export default `
  type Mutation {
    ${authMutationTypes}
    ${userMoviesMutationTypes}
  }
`
