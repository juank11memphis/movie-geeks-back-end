import queryTypes from './queries'
import mutationsTypes from './mutations'
import { sharedTypes } from './shared-types'
import { userType } from '../../components/users/users.graph'
import { authType } from '../../components/auth/auth.graph'
import { userRoleType } from '../../components/user-roles/user-roles.graph'
import { movieType } from '../../components/movies/movies.graph'
import { searchType } from '../../components/search/search.graph'
import { userMovieType } from '../../components/user-movies/user-movies.graph'
import { discoverType } from '../../components/discover/discover.graph'

export default `
  ${queryTypes}
  ${mutationsTypes}
  ${sharedTypes}
  ${authType}
  ${userRoleType}
  ${userType}
  ${movieType}
  ${searchType}
  ${userMovieType}
  ${discoverType}
`
