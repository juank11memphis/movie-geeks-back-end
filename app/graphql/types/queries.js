import { usersQueryTypes } from '../../components/users/users.graph'
import { moviesQueryTypes } from '../../components/movies/movies.graph'
import { searchQueryTypes } from '../../components/search/search.graph'
import { userMoviesQueryTypes } from '../../components/user-movies/user-movies.graph'
import { discoverQueryTypes } from '../../components/discover/discover.graph'

export default `
  type Query {
    ${usersQueryTypes}
    ${moviesQueryTypes}
    ${searchQueryTypes}
    ${userMoviesQueryTypes}
    ${discoverQueryTypes}
  }
`
