# movie-geeks-back-end

MovieGeeks BackEnd

## Requirements

- Install the `aws-cli`
- Run `aws configure` and set the `AWS_ACCESS_KEY_ID` and the `AWS_SECRET_ACCESS_KEY` values with a user that has permissions for doing all the terraform stuff

## How to setup your mongo database

We seed the mongo database using a json file for each collection and using the `mongoimport` command.

- Add one json file for each of your collections and put some data on each file
- Update the file `mongo-seed.sh` and for each json file add a mongoimport command as the one that is already there. Be sure to point the commands to your database by changing the `my-db` part with your mongo database name

## How to run locally

- Run `make local`
- Open `http://localhost:3001/graphql` in your browser to explore the GraphQL Playground

## How to run tests

- Run `make test`
- Run `make test-watch` for watching test files and run the tests again every time a change is detected

## How to test the production build locally

- Run `make prod`

## How to deploy to ecs

- Run `make deploy-ecs-prod` to deploy a new version to the production environment
