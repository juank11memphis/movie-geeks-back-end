#!/usr/bin/env bash
if [ "$ENVIRONMENT" = "local" ]
then
  yarn start
fi

if [ "$ENVIRONMENT" = "test" ]
then
  yarn test
fi

if [ "$ENVIRONMENT" = "server-test" ]
then
  yarn server:test
fi

if [ "$ENVIRONMENT" = "test-watch" ]
then
  yarn test:watch
fi

if [ "$ENVIRONMENT" = "production" ]
then
  yarn build
  yarn start:prod
fi
