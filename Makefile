# ----------------------------
# RUNNING THE APP LOCALLY
# ----------------------------

local: docker-compose-down init
	ENVIRONMENT=local AWS_REGION=us-east-1 docker-compose up --build -d backend
	$(call copy_yarn_lock_file)
	$(call run_docker_logs)

test: docker-compose-down init
	ENVIRONMENT=test docker-compose up --build -d backend
	$(call copy_yarn_lock_file)
	$(call run_docker_logs)

test-watch: docker-compose-down init
	ENVIRONMENT=test-watch docker-compose up --build -d backend
	$(call copy_yarn_lock_file)
	$(call run_docker_logs)

test-server: docker-compose-down init
	ENVIRONMENT=server-test docker-compose up --build -d backend
	$(call copy_yarn_lock_file)
	$(call run_docker_logs)

prod-local: docker-compose-down init
	ENVIRONMENT=production AWS_REGION=us-east-1 docker-compose up --build -d backend
	$(call copy_yarn_lock_file)
	$(call run_docker_logs)

# ---------------------------------
# RUNNING ON THE APP SERVER
# ---------------------------------

deploy-prod: init
	docker build -t prod-back-end .
	docker stop prod-back-end || true
	docker run --rm -d \
		-e ENVIRONMENT=production \
		-e AWS_REGION=us-east-1 \
		-v ~/.aws/:/root/.aws \
		--name prod-back-end \
		--network="host" prod-back-end

deploy-prod-debug: init
	docker build -t prod-back-end .
	docker stop prod-back-end || true
	docker run --rm -d \
		-e ENVIRONMENT=production \
		-e AWS_REGION=us-east-1 \
		-v ~/.aws/:/root/.aws \
		--name prod-back-end \
		--network="host" prod-back-end
	docker logs -f prod-back-end

# ----------------------------
# DOCKER COMMANDS
# ----------------------------

docker-clean-up:
	docker stop mongodb || true
	docker stop mongo-seed || true
	docker stop cache || true
	docker stop backend || true
	docker rm -v $(shell docker ps -a -q -f status=exited) 2>&1 || true
	docker rmi $(shell docker images -f "dangling=true" -q) 2>&1 || true
	docker rm mongodb || true
	docker rm mongo-seed || true
	docker rm cache || true
	docker rm backend || true
	docker volume prune -f

docker-compose-down: docker-clean-up
	docker-compose down --remove-orphans

# ----------------------------
# UTILITY FUNCTIONS
# ----------------------------

init:
	$(call init_yarn_cache)
	$(call create_network)

define create_network
	docker network create moviegeeks || true
endef

define init_yarn_cache
	# Init empty cache file if needed
	if [ ! -f .yarn-cache.tgz ]; then \
		echo "Init empty .yarn-cache.tgz"; \
		tar cvzf .yarn-cache.tgz --files-from /dev/null; \
	fi;
endef

define copy_yarn_lock_file
	# we copy the yarn.lock file back from the container only if it has changed
	docker cp backend:/tmp/yarn.lock ./yarn-tmp.lock
	if ! diff -q yarn.lock yarn-tmp.lock > /dev/null  2>&1; then \
		cp yarn-tmp.lock yarn.lock; \
		docker cp backend:/tmp/.yarn-cache.tgz ./.yarn-cache.tgz; \
	fi;
	rm yarn-tmp.lock
endef

define run_docker_logs
	docker-compose logs -f mongo-seed
	docker-compose logs -f backend
endef

# ----------------------------
# MAKE TARGETS
# ----------------------------

.PHONY: docker-compose-down docker-clean-up init
