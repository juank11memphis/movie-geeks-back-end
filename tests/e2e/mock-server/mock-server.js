import { ApolloServer, AuthenticationError } from 'apollo-server'
import { has } from 'lodash'

import typeDefs from '../../../app/graphql/types'
import getPlayingNowFixtures from './fixtures/queries/getPlayingNow'
import loadAuthUserFixtures from './fixtures/queries/loadAuthUser'
import signInFixtures from './fixtures/mutations/signin'
import signUpFixtures from './fixtures/mutations/signup'
import requestPasswordResetFixtures from './fixtures/mutations/requestPasswordReset'
import resetPasswordFixtures from './fixtures/mutations/resetPassword'
import searchFixtures from './fixtures/queries/search'
import addToWatchlistFixtures from './fixtures/mutations/addToWatchlist'
import removeFromWatchlistFixtures from './fixtures/mutations/removeFromWatchlist'
import loadWatchlistFixtures from './fixtures/queries/loadWatchlist'
import loadWatchlistAsFixtures from './fixtures/queries/loadWatchlistAs'
import loadUserByIdFixtures from './fixtures/queries/loadUserById'
import removeMovieRatingFixtures from './fixtures/mutations/removeMovieRating'
import rateMovieFixtures from './fixtures/mutations/rateMovie'
import loadCollectionFixtures from './fixtures/queries/loadCollection'
import loadCollectionAsFixtures from './fixtures/queries/loadCollectionAs'
import discoverFixtures from './fixtures/queries/discover'
import getYoutubeTrailerFixtures from './fixtures/queries/getYoutubeOfficialTrailerKey'

export const startMockServer = () => {
  const isLoggedIn = context => {
    const { headers } = context
    const isLoggedIn = headers['x-test-logged-in'] || false
    if (!isLoggedIn || isLoggedIn === 'false' || isLoggedIn === 'null') {
      return false
    }
    return true
  }

  const testResolverHandler = (context, fixtures, serviceName) => {
    const { headers } = context
    const responseMode = headers['x-test-response-mode'] || 'default'
    if (responseMode === 'error') {
      throw new Error('Mock Server Error')
    }
    if (responseMode === 'no-data') {
      return null
    }
    const dataOverrides = headers['x-test-service-data-override']
      ? JSON.parse(headers['x-test-service-data-override'])
      : []
    const serviceOverride = (dataOverrides || []).find(
      dataOverride => dataOverride.service === serviceName,
    )
    if (serviceName && serviceOverride) {
      return serviceOverride.data
    }
    const loggedIn = isLoggedIn(context)
    if (loggedIn && has(fixtures, 'loggedIn')) {
      return fixtures.loggedIn
    }
    if (has(fixtures, 'default')) {
      return fixtures.default
    }
    return fixtures
  }

  const testAuthResolverHandler = (context, fixtures) => {
    if (!isLoggedIn(context)) {
      throw new AuthenticationError('Unauthorized!!')
    }
    return testResolverHandler(context, fixtures)
  }

  const resolvers = {
    Query: {
      getPlayingNow: (_, args, context) =>
        testResolverHandler(context, getPlayingNowFixtures),
      loadAuthUser: (_, args, context) =>
        testAuthResolverHandler(context, loadAuthUserFixtures),
      search: (_, args, context) =>
        testResolverHandler(context, searchFixtures),
      loadWatchlist: (_, args, context) =>
        testResolverHandler(context, loadWatchlistFixtures, 'loadWatchlist'),
      loadWatchlistAs: (_, args, context) =>
        testResolverHandler(context, loadWatchlistAsFixtures),
      loadCollection: (_, args, context) =>
        testResolverHandler(context, loadCollectionFixtures, 'loadCollection'),
      loadCollectionAs: (_, args, context) =>
        testResolverHandler(
          context,
          loadCollectionAsFixtures,
          'loadCollectionAs',
        ),
      loadUserById: (_, args, context) =>
        testResolverHandler(context, loadUserByIdFixtures, 'loadUserById'),
      discover: (_, args, context) =>
        testResolverHandler(context, discoverFixtures, 'discover'),
      getYoutubeOfficialTrailerKey: (_, args, context) =>
        testResolverHandler(
          context,
          getYoutubeTrailerFixtures,
          'getYoutubeOfficialTrailerKey',
        ),
    },
    Mutation: {
      signin: (_, args, context) =>
        testResolverHandler(context, signInFixtures),
      signup: (_, args, context) =>
        testResolverHandler(context, signUpFixtures),
      requestPasswordReset: (_, args, context) =>
        testResolverHandler(context, requestPasswordResetFixtures),
      resetPassword: (_, args, context) =>
        testResolverHandler(context, resetPasswordFixtures),
      addToWatchlist: (_, args, context) =>
        testResolverHandler(context, addToWatchlistFixtures),
      removeFromWatchlist: (_, args, context) =>
        testResolverHandler(context, removeFromWatchlistFixtures),
      removeMovieRating: (_, args, context) =>
        testResolverHandler(context, removeMovieRatingFixtures),
      rateMovie: (_, args, context) =>
        testResolverHandler(context, rateMovieFixtures),
      refreshToken: (_, args, context) =>
        testAuthResolverHandler(context, signInFixtures),
    },
  }

  const server = new ApolloServer({
    typeDefs,
    resolvers,
    context: ({ req, res }) => ({
      headers: req.headers,
    }),
  })

  server.listen(3001).then(({ url }) => {
    console.log(`🚀 Server ready at ${url}`)
  })
}
