require('@babel/register')
const startMockServer = require('./mock-server').startMockServer

if (process.env.NODE_ENV === 'server-test') {
  startMockServer()
}
