export default {
  userMovies: [
    {
      id: 665544,
      userId: null,
      movieId: '346910',
      rating: null,
      saved: false,
      movie: {
        id: 346910,
        title: 'The Predator',
        originalTitle: 'The Predator',
        posterPath: 'wMq9kQXTeQCHUZOG4fAe5cAxyUA.jpg',
        genres: ['Horror', 'Science Fiction', 'Action', 'Thriller'],
        overview:
          'From the outer reaches of space to the small-town streets of suburbia, the hunt comes home. Now, the universe’s most lethal hunters are stronger, smarter and deadlier than ever before, having genetically upgraded themselves with DNA from other species. When a young boy accidentally triggers their return to Earth, only a ragtag crew of ex-soldiers and a disgruntled science teacher can prevent the end of the human race.',
        releaseDate: '2018-09-14',
        year: '2018',
      },
      comparandUser: {
        userId: '11',
        rating: 7,
        saved: false,
      },
    },
  ],
  pagingMetadata: {
    next: null,
    hasNext: false,
  },
}
