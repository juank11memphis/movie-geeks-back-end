export default {
  default: {
    items: [],
  },
  loggedIn: {
    items: [
      {
        id: '580100f4da893943d393e248',
        movieId: '580100f4da893943d393e248',
        averageRating: 9.5,
        totalRating: 28.5,
        count: 3,
        movie: {
          id: '580100f4da893943d393e248',
          title: 'What We Do in the Shadows',
          originalTitle: 'What We Do in the Shadows',
          posterPath: '/4bqqrtzY9vVQeEcqyqWfTJWmpD9.jpg',
          genres: ['Comedy', 'Horror'],
          overview:
            'Vampire housemates try to cope with the complexities of modern life and show a newly turned hipster some of the perks of being undead.',
          releaseDate: '2014-06-19',
          year: '2014',
          youtubeTrailerKey: null,
        },
      },
      {
        id: '580100f4da893943d393e249',
        movieId: '580100f4da893943d393e249',
        averageRating: 9.333333333333334,
        totalRating: 28,
        count: 3,
        movie: {
          id: '580100f4da893943d393e249',
          title: 'Point Blank',
          originalTitle: 'À bout portant',
          posterPath: '/dJVkWNpuLYG0ogZesOPMeHN2M7k.jpg',
          genres: ['Action', 'Thriller', 'Crime'],
          overview:
            "Samuel Pierret is a nurse who saves the wrong guy – a thief whose henchmen take Samuel's pregnant wife hostage to force him to spring their boss from the hospital. A race through the subways and streets of Paris ensues, and the body count rises. Can Samuel evade the cops and the criminal underground and deliver his beloved to safety?",
          releaseDate: '2010-12-01',
          year: '2010',
          youtubeTrailerKey: null,
        },
      },
      {
        id: '580100f4da893943d393e247',
        movieId: '580100f4da893943d393e247',
        averageRating: 9.25,
        totalRating: 18.5,
        count: 2,
        movie: {
          id: '580100f4da893943d393e247',
          title: 'The Wife',
          originalTitle: 'The Wife',
          posterPath: '/d4Qyuy0Ul549f6SOdUqGvIdYKD2.jpg',
          genres: ['Drama'],
          overview:
            'A wife questions her life choices as she travels to Stockholm with her husband, where he is slated to receive the Nobel Prize for Literature.',
          releaseDate: '2018-08-02',
          year: '2018',
          youtubeTrailerKey: null,
        },
      },
      {
        id: '580100f4da893943d393e250',
        movieId: '580100f4da893943d393e250',
        averageRating: 9.166666666666666,
        totalRating: 27.5,
        count: 3,
        movie: {
          id: '580100f4da893943d393e250',
          title: 'The Cakemaker',
          originalTitle: 'האופה מברלין',
          posterPath: '/WwHLnuqqwhvCkBACCMAR5xuQMA.jpg',
          genres: ['Drama'],
          overview:
            'Thomas, a young German baker, is having an affair with Oren, an Israeli married man who has frequent business visits in Berlin. When Oren dies in a car crash in Israel, Thomas travels to Jerusalem seeking for answers regarding his death. Under a fabricated identity, Thomas infiltrates the life of Anat, his lover’s newly widowed wife, who owns a small Café in downtown Jerusalem. Thomas starts to work for her, creating German cakes and cookies that bring her Café to life. Thomas finds himself involved in Anat’s life in a way far beyond his anticipation. To protect the truth he will stretch his lie to a point of no return.',
          releaseDate: '2017-12-28',
          year: '2017',
          youtubeTrailerKey: null,
        },
      },
    ],
  },
}
