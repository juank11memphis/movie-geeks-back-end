export default {
  userMovies: [
    {
      id: 335983,
      userId: null,
      movieId: '335983',
      rating: null,
      saved: false,
      movie: {
        id: 335983,
        title: 'Venom',
        originalTitle: 'Venom',
        posterPath: '2uNW4WbgBXL25BAbXGLnLqX71Sw.jpg',
        genres: ['Science Fiction', 'Action', 'Comedy', 'Crime'],
        overview:
          'When Eddie Brock acquires the powers of a symbiote, he will have to release his alter-ego “Venom” to save his life.',
        releaseDate: '2018-10-05',
        year: '2018',
      },
      comparandUser: {
        userId: '11',
        rating: null,
        saved: true,
      },
    },
    {
      id: 112233,
      userId: null,
      movieId: '369972',
      rating: null,
      saved: false,
      movie: {
        id: 369972,
        title: 'First Man',
        originalTitle: 'First Man',
        posterPath: 'i91mfvFcPPlaegcbOyjGgiWfZzh.jpg',
        genres: ['History', 'Drama'],
        overview:
          'A look at the life of the astronaut, Neil Armstrong, and the legendary space mission that led him to become the first man to walk on the Moon on July 20, 1969.',
        releaseDate: '2018-10-12',
        year: '2018',
      },
      comparandUser: {
        userId: '11',
        rating: null,
        saved: true,
      },
    },
  ],
  pagingMetadata: {
    next: null,
    hasNext: false,
  },
}
