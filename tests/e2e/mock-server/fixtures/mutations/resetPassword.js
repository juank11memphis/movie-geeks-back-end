module.exports = {
  token: 'an_awesome_token',
  user: {
    id: 11,
    firstname: 'Juan',
    lastname: 'Morales',
    email: 'juan@gmail.com',
    password: 'a_good_and_strong_password',
    hasPassword: true,
  },
}
