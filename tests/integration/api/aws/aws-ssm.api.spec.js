import aws from 'aws-sdk'
import { expect } from 'chai'
import sinon from 'sinon'

import awsParametersStore from '../../../../app/api/aws/aws-ssm.api'

const sandbox = sinon.createSandbox()

describe('AWS SSM Api', () => {
  beforeEach(() => {
    sandbox.stub(aws.config, 'update').returns(true)
  })

  afterEach(() => {
    sandbox.restore()
  })

  it('should load parameters by path', async () => {
    const getParametersByPathStub = (aws.SSM.prototype.getParametersByPath = sandbox.stub())
    getParametersByPathStub.yields(null, { Parameters: [{ key: 'value' }] })
    const values = await awsParametersStore.getParametersByPath('a-path')
    expect(values).to.deep.equal([{ key: 'value' }])
  })

  it('should load parameters by path passing a next token', async () => {
    const getParametersByPathStub = (aws.SSM.prototype.getParametersByPath = sandbox.stub())
    getParametersByPathStub.yields(null, { Parameters: [{ key: 'value' }] })
    const values = await awsParametersStore.getParametersByPath(
      'a-path',
      'a-next-token',
    )
    expect(values).to.deep.equal([{ key: 'value' }])
  })

  it('should load parameters when aws result contains a NextToken', async () => {
    const getParametersByPathStub = sandbox.stub(
      awsParametersStore,
      'getParametersByPathAsPromise',
    )
    getParametersByPathStub.withArgs('a-path', null).returns({
      Parameters: [{ keyOne: 'valueOne' }],
      NextToken: 'a-next-token',
    })

    getParametersByPathStub
      .withArgs('a-path', 'a-next-token')
      .returns({ Parameters: [{ keyTwo: 'valueTwo' }] })

    const values = await awsParametersStore.getParametersByPath('a-path', null)
    expect(values).to.deep.equal([
      { keyOne: 'valueOne' },
      { keyTwo: 'valueTwo' },
    ])
  })

  it('should load parameters by path when errors happen', async () => {
    const getParametersByPathStub = (aws.SSM.prototype.getParametersByPath = sandbox.stub())
    getParametersByPathStub.yields(new Error('Test Error'), null)
    const values = await awsParametersStore.getParametersByPath('a-path')
    expect(values).to.deep.equal([])
  })
})
