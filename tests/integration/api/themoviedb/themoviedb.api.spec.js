import { expect } from 'chai'
import sinon from 'sinon'
import request from 'request'

import themoviedbApi from '../../../../app/api/themoviedb/themoviedb.api'
import {
  expectedSearchResultApi,
  expectedSearchResultsNoData,
} from '../../snapshots/search'
import { expectedPlayingNowResultsApi } from '../../snapshots/movies'
import {
  themoviedbApiSearchResponse,
  themoviedbApiSearchResponseNoData,
  themoviedbApiVideosResponse,
  themoviedbApiBadVideosResponse,
} from '../../mock-data/themoviedb.mocks'

const sandbox = sinon.createSandbox()

describe('TheMovieDB Api', () => {
  afterEach(() => {
    sandbox.restore()
  })

  it('should search movies', async () => {
    sandbox
      .stub(request, 'get')
      .yields(null, { statusCode: 200 }, themoviedbApiSearchResponse)
    const searchResult = await themoviedbApi.searchMovies('fight')
    expect(searchResult).to.deep.equal(expectedSearchResultApi)
  })

  it('should handle search movies when search returns no data', async () => {
    sandbox
      .stub(request, 'get')
      .yields(null, { statusCode: 200 }, themoviedbApiSearchResponseNoData)
    const searchResult = await themoviedbApi.searchMovies('fight')
    expect(searchResult).to.deep.equal(expectedSearchResultsNoData)
  })

  it('should get playing now movies', async () => {
    sandbox
      .stub(request, 'get')
      .yields(null, { statusCode: 200 }, themoviedbApiSearchResponse)
    const searchResult = await themoviedbApi.getPlayingNow()
    expect(searchResult).to.deep.equal(expectedPlayingNowResultsApi)
  })

  it('should get movies official youtube trailer key', async () => {
    sandbox
      .stub(request, 'get')
      .yields(null, { statusCode: 200 }, themoviedbApiVideosResponse)
    const key = await themoviedbApi.getOfficialTrailerYoutubeKey('320288')
    expect(key).to.deep.equal('QWbMckU3AOQ')
  })

  it('should return first trailer found when an official trailer is not found', async () => {
    sandbox
      .stub(request, 'get')
      .yields(null, { statusCode: 200 }, themoviedbApiBadVideosResponse)
    const key = await themoviedbApi.getOfficialTrailerYoutubeKey('320288')
    expect(key).to.deep.equal('QWbMckU3AOQ')
  })

  it('should return proper urls', () => {
    const url = themoviedbApi.getUrl('somequery?someParam=1')
    expect(url).to.deep.equal(
      'https://api.themoviedb.org/3/somequery?someParam=1&api_key=a_movide_db_key',
    )
    const url2 = themoviedbApi.getUrl('somequery')
    expect(url2).to.deep.equal(
      'https://api.themoviedb.org/3/somequery?api_key=a_movide_db_key',
    )
  })

  it('should handle parsing movies when they dont have all the data', () => {
    const movies = [{ id: 1, title: 'movie1' }]
    const parsedMovies = themoviedbApi.parseMovies(movies)
    expect(parsedMovies[0]).to.deep.equal({
      id: 1,
      title: 'movie1',
      originalTitle: undefined,
      posterPath: null,
      genres: [],
      overview: undefined,
      releaseDate: undefined,
      year: '',
    })
  })
})
