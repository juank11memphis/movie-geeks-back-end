import { expect } from 'chai'
import sinon from 'sinon'
import Mailgun from 'mailgun-js'
import MailComposer from 'nodemailer/lib/mail-composer'

import * as MailgunUtil from '../../../app/util/mailgun.util'

const sandbox = sinon.createSandbox()
const mailgunTestValues = {
  apiKey: 'foo',
  domain: 'bar',
}

describe('Mailgun Util', () => {
  before(() => {
    sandbox
      .stub(Mailgun(mailgunTestValues).Mailgun.prototype, 'messages')
      .returns({
        sendMime: (data, cb) => cb(null, true),
      })
  })

  after(() => {
    sandbox.restore()
  })

  it('should send welcome emails', async () => {
    const result = await MailgunUtil.sendWelcomeEmail({
      firstname: 'Juan',
      lastname: 'Morales',
      email: 'test@gmail.com',
    })

    expect(result).to.equal(true)
  })

  it('should send forgot password emails', async () => {
    const result = await MailgunUtil.sendResetPasswordEmail({
      email: 'test@gmail.com',
      link: 'an_awesome_reset_password_link',
    })
    expect(result).to.equal(true)
  })

  it('should handle errors on compose email', async () => {
    const tmpSandbox = sinon.createSandbox()
    tmpSandbox.stub(MailComposer.prototype, 'compile').returns({
      build: callback => callback('an error'),
    })
    try {
      await MailgunUtil.composeEmail({})
    } catch (error) {
      expect(error).to.deep.equal('an error')
    }
    tmpSandbox.restore()
    try {
      await MailgunUtil.composeEmail(null)
    } catch (error) {
      expect(error.message).to.equal("Cannot read property 'to' of null")
    }
  })

  it('should handle errors sending email on compose email', async () => {
    sandbox.restore()
    const tmpSandbox = sinon.createSandbox()
    tmpSandbox.stub(MailComposer.prototype, 'compile').returns({
      build: callback => callback(null, 'a message'),
    })
    tmpSandbox
      .stub(Mailgun(mailgunTestValues).Mailgun.prototype, 'messages')
      .returns({
        sendMime: (data, cb) => cb('an error'),
      })
    try {
      await MailgunUtil.composeEmail({})
    } catch (error) {
      expect(error).to.deep.equal('an error')
    }
    tmpSandbox.restore()
  })

  it('should handle errors on send welcome email', async () => {
    const tmpSandbox = sinon.createSandbox()
    tmpSandbox.stub(MailComposer.prototype, 'compile').returns({
      build: callback => callback('an error'),
    })
    let result
    try {
      result = await MailgunUtil.sendWelcomeEmail({})
    } catch (error) {
      expect(error).to.deep.equal('an error')
      expect(result).to.equal(true)
    }
    tmpSandbox.restore()
  })

  it('should handle errors on send reset password email', async () => {
    const tmpSandbox = sinon.createSandbox()
    tmpSandbox.stub(MailComposer.prototype, 'compile').returns({
      build: callback => callback('an error'),
    })
    let result
    try {
      result = await MailgunUtil.sendResetPasswordEmail({})
    } catch (error) {
      expect(error).to.deep.equal('an error')
      expect(result).to.equal(true)
    }
    tmpSandbox.restore()
  })

  it('should handle errors on send email', async () => {
    try {
      await MailgunUtil.sendEmail(null)
    } catch (error) {
      expect(error.message).to.equal("Cannot read property 'to' of null")
    }
  })
})
