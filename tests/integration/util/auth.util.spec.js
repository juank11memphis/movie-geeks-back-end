import { expect } from 'chai'

import {
  generateToken,
  validateToken,
  getTokenData,
  validatePermissions,
  hashPassword,
  comparePasswords,
} from '../../../app/util/auth.util'

const testUser = {
  _id: 1,
  name: 'Juan Carlos',
  roles: [
    {
      _id: '1',
      name: 'admins',
      permissions: ['create:quote', 'update:footer'],
    },
    {
      _id: '2',
      name: 'Movie Geeks',
      permissions: ['update:movie', 'update:artist'],
    },
  ],
}

const testUser2 = {
  _id: 1,
  name: 'Sandra',
  roles: [
    {
      _id: '1',
      name: 'admins',
      permissions: ['create:quote', 'update:footer'],
    },
    {
      _id: '2',
      name: 'Designer',
      permissions: ['update:styles'],
    },
  ],
}

describe('AuthUtil', () => {
  it('should decode valid tokens', async () => {
    const token = await generateToken(testUser)
    const decodedToken = await validateToken(`bearer ${token}`)
    expect(decodedToken.userId).to.equal(testUser._id)
    expect(decodedToken.userRoles).to.containSubset(testUser.roles)
  })

  it('should throw error if token is not provided', async () => {
    try {
      const decodedToken = await validateToken()
    } catch (error) {
      expect(error.message).to.equal('Unauthorized!!')
    }
  })

  it('should throw error if token is not a bearer token', async () => {
    try {
      const decodedToken = await validateToken('not a bearer token')
    } catch (error) {
      expect(error.message).to.equal('Unauthorized!!')
    }
  })

  it('should throw error if an invalid token is provided', async () => {
    try {
      const decodedToken = await validateToken('bearer a bad token')
    } catch (error) {
      expect(error.message).to.equal('Unauthorized!!')
    }
  })

  it('should throw not enough permissions error if user has no roles', () => {
    try {
      const hasPermissions = validatePermissions(null, 'update:movie')
    } catch (error) {
      expect(error.message).to.equal('Not enough permissions')
    }
    try {
      const hasPermissions = validatePermissions([], 'update:movie')
    } catch (error) {
      expect(error.message).to.equal('Not enough permissions')
    }
  })

  it('should throw not enough permissions error if user roles have invalid permissions', () => {
    try {
      const hasPermissions = validatePermissions(
        [{ permissions: null }],
        'update:movie',
      )
    } catch (error) {
      expect(error.message).to.equal('Not enough permissions')
    }
    try {
      const hasPermissions = validatePermissions(
        [{ permissions: [] }],
        'update:movie',
      )
    } catch (error) {
      expect(error.message).to.equal('Not enough permissions')
    }
  })

  it('should throw not enough permissions error if user dont have a specific permission', () => {
    try {
      const hasPermissions = validatePermissions(
        testUser2.roles,
        'update:movie',
      )
    } catch (error) {
      expect(error.message).to.equal('Not enough permissions')
    }
  })

  it('should return true if user has a specific permission', () => {
    const hasPermissions = validatePermissions(testUser.roles, 'update:movie')
    expect(hasPermissions).to.equal(true)
  })

  it('should return null if invalid token is provided', async () => {
    const decodedToken = await getTokenData('bearer a bad token')
    expect(decodedToken).to.equal(null)
  })

  it('should return tokenData if valid token is provided', async () => {
    const token = await generateToken(testUser)
    const decodedToken = await getTokenData(`bearer ${token}`)
    expect(decodedToken.userId).to.equal(testUser._id)
    expect(decodedToken.userRoles).to.containSubset(testUser.roles)
  })

  it('should hash a password', () => {
    expect(comparePasswords('password', hashPassword('password'))).to.equal(
      true,
    )
  })
})
