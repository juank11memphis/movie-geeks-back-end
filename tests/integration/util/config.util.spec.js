import { expect } from 'chai'
import sinon from 'sinon'

import configUtil from '../../../app/util/config.util'
import parametersStore from '../../../app/api/aws/aws-ssm.api'

const sandbox = sinon.createSandbox()

describe('Config Util', () => {
  afterEach(() => {
    sandbox.restore()
    process.env.NODE_ENV = 'test'
  })
  it('should initialize configurat2ion when process.env.NODE_ENV is not set', async () => {
    delete process.env.NODE_ENV
    const values = configUtil.init()
    expect(values).to.be.empty
  })

  it('should initialize configuration when process.env.NODE_ENV is test', async () => {
    process.env.NODE_ENV = 'test'
    const values = configUtil.init()
    expect(values).to.be.empty
  })

  it('should initialize configuration when process.env.NODE_ENV is not local and not test', async () => {
    process.env.NODE_ENV = 'production'
    sandbox
      .stub(parametersStore, 'getParametersByPath')
      .returns([
        { Name: 'prop1', Value: 'value1' },
        { Name: 'prop2', Value: 'value2' },
      ])
    const values = await configUtil.init()
    expect(values).to.deep.equal({ prop1: 'value1', prop2: 'value2' })
  })

  it('should get app version', () => {
    const version = configUtil.getVersion()
    expect(version).not.to.be.empty
  })

  it('should return value from detaults when NODE_ENV is test', () => {
    process.env.NODE_ENV = 'test'
    const value = configUtil.getValue('port')
    expect(value).to.equal(5678)
  })

  it('should return value from process.env if it contains it', () => {
    process.env.NODE_ENV = 'production'
    process.env['test'] = '11'
    const value = configUtil.getValue('test')
    expect(value).to.equal('11')
  })

  it('should return value as integer', () => {
    process.env.NODE_ENV = 'production'
    process.env['test'] = 11
    const value = configUtil.getValueAsInt('test')
    expect(value).to.equal(11)
  })

  it('should return value from values loaded from the AWS parameters store', async () => {
    process.env.NODE_ENV = 'production'
    sandbox
      .stub(parametersStore, 'getParametersByPath')
      .returns([
        { Name: 'prop1', Value: 'value1' },
        { Name: 'prop2', Value: 'value2' },
      ])
    const values = await configUtil.init()
    const value = configUtil.getValue('prop1')
    expect(value).to.equal('value1')
  })

  it('should know if current env is production or not', () => {
    process.env.NODE_ENV = 'production'
    const isProd = configUtil.isProd()
    expect(isProd).to.equal(true)
  })
})
