export const themoviedbApiPlayingNowResponse = [
  {
    id: 550,
    title: 'The Fight Club',
    originalTitle: 'The Fight Club',
    posterPath: 'adw6Lq9FiC9zjYEpOqfq03ituwp.jpg',
    genres: ['Drama'],
    overview: 'A ticking-time-bomb...',
    releaseDate: '1999-10-15',
    year: '1999',
  },
  {
    id: 482986,
    title: 'The Fight',
    originalTitle: 'The Fight',
    posterPath: 'uXrvRFcy7YJSyhtPpeaxKdXYHd3.jpg',
    genres: ['Comedy', 'Drama'],
    overview: 'Tina lives in a quiet seaside...',
    releaseDate: '2018-10-17',
    year: '2018',
  },
]

export const themoviedbApiSearchResponse = {
  page: 1,
  total_results: 1232,
  total_pages: 62,
  results: [
    {
      id: 550,
      title: 'The Fight Club',
      original_title: 'The Fight Club',
      poster_path: 'adw6Lq9FiC9zjYEpOqfq03ituwp.jpg',
      genre_ids: [18],
      overview: 'A ticking-time-bomb...',
      release_date: '1999-10-15',
    },
    {
      id: 482986,
      title: 'The Fight',
      original_title: 'The Fight',
      poster_path: 'uXrvRFcy7YJSyhtPpeaxKdXYHd3.jpg',
      genre_ids: [35, 18],
      overview: 'Tina lives in a quiet seaside...',
      release_date: '2018-10-17',
    },
  ],
}

export const themoviedbApiSearchResponseNoData = {
  page: 1,
  total_results: 0,
  total_pages: 0,
  results: [],
}

export const themoviedbApiParsedSearchResponse = {
  page: 1,
  totalResults: 1232,
  totalPages: 62,
  movies: [
    {
      id: 550,
      title: 'The Fight Club',
      originalTitle: 'The Fight Club',
      posterPath: 'adw6Lq9FiC9zjYEpOqfq03ituwp.jpg',
      genres: ['Drama'],
      overview: 'A ticking-time-bomb...',
      releaseDate: '1999-10-15',
      year: '1999',
    },
    {
      id: 482986,
      title: 'The Fight',
      originalTitle: 'The Fight',
      posterPath: 'uXrvRFcy7YJSyhtPpeaxKdXYHd3.jpg',
      genres: ['Comedy', 'Drama'],
      overview: 'Tina lives in a quiet seaside...',
      releaseDate: '2018-10-17',
      year: '2018',
    },
  ],
}

export const themoviedbApiVideosResponse = {
  id: 320288,
  results: [
    {
      id: '5bac7056c3a3683aa203351c',
      iso_639_1: 'en',
      iso_3166_1: 'US',
      key: 'QWbMckU3AOQ',
      name: 'Dark Phoenix | Official Trailer [HD] | 20th Century FOX',
      site: 'YouTube',
      size: 1080,
      type: 'Trailer',
    },
    {
      id: '5c7fd1180e0a2643015c451c',
      iso_639_1: 'en',
      iso_3166_1: 'US',
      key: 'Im4odVLNxqo',
      name: 'X-men: Dark Phoenix - International Trailer',
      site: 'YouTube',
      size: 1080,
      type: 'Trailer',
    },
    {
      id: '5c9cdf4792514124cf31070a',
      iso_639_1: 'en',
      iso_3166_1: 'US',
      key: '1-q8C_c-nlM',
      name: 'Dark Phoenix | Official Trailer [HD] | 20th Century FOX',
      site: 'YouTube',
      size: 1080,
      type: 'Trailer',
    },
    {
      id: '5cab8f29c3a36860c2b45c08',
      iso_639_1: 'en',
      iso_3166_1: 'US',
      key: 'IqflOkaT5bI',
      name:
        'Dark Phoenix | "Every Hero Has A Dark Side" TV Commercial | 20th Century FOX',
      site: 'YouTube',
      size: 1080,
      type: 'Teaser',
    },
    {
      id: '5cb74307c3a3686b0783e107',
      iso_639_1: 'en',
      iso_3166_1: 'US',
      key: 'azvR__GRQic',
      name: 'Dark Phoenix | Final Trailer [HD] | 20th Century FOX',
      site: 'YouTube',
      size: 1080,
      type: 'Trailer',
    },
  ],
}

export const themoviedbApiBadVideosResponse = {
  id: 320288,
  results: [
    {
      id: '5bac7056c3a3683aa203351c',
      iso_639_1: 'en',
      iso_3166_1: 'US',
      key: 'QWbMckU3AOQ',
      name: 'Dark Phoenix | Trailer [HD] | 20th Century FOX',
      site: 'YouTube',
      size: 1080,
      type: 'Trailer',
    },
    {
      id: '5c7fd1180e0a2643015c451c',
      iso_639_1: 'en',
      iso_3166_1: 'US',
      key: 'Im4odVLNxqo',
      name: 'X-men: Dark Phoenix - International Trailer',
      site: 'YouTube',
      size: 1080,
      type: 'Trailer',
    },
  ],
}
