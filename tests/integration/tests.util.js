require('dotenv').config()
import mongoose from 'mongoose'
import chai from 'chai'
import chaiSubset from 'chai-subset'
import Fixtures from 'node-mongodb-fixtures'

import configUtil from '../../app/util/config.util'

chai.use(chaiSubset)
mongoose.Promise = global.Promise

const fixtures = new Fixtures({
  dir: 'tests/integration/db-fixtures',
  mute: true,
})

export const connectMongoose = () => {
  const promise = async (resolve, reject) => {
    try {
      const alreadyConnected =
        mongoose.connection.db !== undefined && mongoose.connection.db !== null
      if (alreadyConnected) {
        resolve()
      } else {
        mongoose.Promise = global.Promise
        mongoose.connect(configUtil.getValue('mongodb_url'), {
          useNewUrlParser: true,
          user: configUtil.getValue('mongo_username'),
          pass: configUtil.getValue('mongo_password'),
        })
        mongoose.connection.once('open', () => {
          resolve()
        })
      }
    } catch (error) {
      reject(error)
    }
  }
  return new Promise(promise)
}

export const testsSetup = async done => {
  try {
    await connectMongoose()
    await fixtures.connect(configUtil.defaults.test.mongodb_url, {
      auth: {
        user: configUtil.getValue('mongo_username'),
        password: configUtil.getValue('mongo_password'),
      },
    })
    await fixtures.unload()
    await fixtures.load()
    await fixtures.disconnect()
    done()
  } catch (error) {
    setTimeout(() => {
      testsSetup(done)
    }, 100)
  }
}
