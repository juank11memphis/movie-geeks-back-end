const ids = require('../fixtures-ids')

module.exports = [
  {
    _id: ids.userUsers.sammetLandeId,
    user: ids.users.sammetId,
    comparandUser: ids.users.landeId,
    score: 1,
    movies: [
      {
        id: ids.movies.bloodDiamondId,
        points: 1,
      },
    ],
    updatedAt: '2018-01-25T02:00:11.959Z',
  },
]
