const ids = require('../fixtures-ids')

module.exports = [
  {
    _id: ids.userRoles.admin,
    name: 'admin',
    permissions: ['create:movie'],
  },
  {
    _id: ids.userRoles.user,
    name: 'user',
    permissions: ['read:movie'],
  },
]
