import { expect } from 'chai'

import { testsSetup } from '../../tests.util'
import User from '../../../../app/components/users/user'

describe('User Model', () => {
  beforeEach(done => {
    testsSetup(done)
  })

  it('should not save a user that has an already existing email', async () => {
    let user = new User({
      firstname: 'Jose',
      email: 'juan@gmail.com',
      password: 'test-password',
    })
    try {
      await user.save()
    } catch (error) {
      expect(error.message).to.equal(
        'User validation failed: email: Email already in use',
      )
    }
  })

  it('should not save a user if an invalid email is provided', async () => {
    let user = new User({
      firstname: 'Jose',
      email: 'josegmail',
      password: 'test-password',
    })
    try {
      await user.save()
    } catch (error) {
      expect(error.message).to.equal(
        'User validation failed: email: josegmail is not a valid email',
      )
    }
  })

  it('should not save a user if no email is provided', async () => {
    let user = new User({
      firstname: 'Jose',
      password: 'test-password',
    })
    try {
      await user.save()
    } catch (error) {
      expect(error.message).to.equal(
        'User validation failed: email: Email is required',
      )
    }
  })

  it('should not save a user if no first name is provided', async () => {
    let user = new User({
      email: 'juan2@gmail.com',
      password: 'test-password',
    })
    try {
      await user.save()
    } catch (error) {
      expect(error.message).to.equal(
        'User validation failed: firstname: First name is required',
      )
    }
  })

  it('should not save a user if password is provided but is smaller than 6 characters', async () => {
    let user = new User({
      firstname: 'Jose',
      email: 'juan2@gmail.com',
      password: 'short',
    })
    try {
      await user.save()
    } catch (error) {
      expect(error.message).to.equal(
        'User validation failed: password: Password must be at least 6 characters long',
      )
    }
  })
})
