import { expect } from 'chai'

import { testsSetup } from '../../tests.util'
import usersService from '../../../../app/components/users/users.service'
import ids from '../../fixtures-ids'
import { expectedJuanUser, expectedJuanUpdated } from '../../snapshots/users'

describe('UsersService', () => {
  beforeEach(done => {
    testsSetup(done)
  })

  it('should load users by ids', async () => {
    const { users } = ids
    const songsLoaded = await usersService.loadByIds([
      users.juanId,
      users.sanId,
    ])
    expect(songsLoaded.length).to.equal(2)
  })

  it('should create an user', async () => {
    const user = await usersService.create({
      firstname: 'Juanca',
      email: 'juan2@gmail.com',
    })
    expect(user._id).not.to.be.undefined
    expect(user.firstname).to.equal('Juanca')
    expect(user.email).to.equal('juan2@gmail.com')
  })

  it('Should load user details by email', async () => {
    const { users } = ids
    const user = await usersService.getDetailByEmail('juan@gmail.com')
    expect(user._id + '').to.deep.equal(users.juanId + '')
    expect(user.firstname).to.equal('Juan')
  })

  it('should load user details', async () => {
    const { users } = ids
    const user = await usersService.getDetailById(users.juanId)
    expect(user).to.containSubset(expectedJuanUser)
  })

  it('should update user', async () => {
    const { users } = ids
    const user = await usersService.updateUser(users.juanId, {
      firstname: 'Carlos',
    })
    expect(user).to.containSubset(expectedJuanUpdated)
    expect(user.updatedAt).not.to.be.undefined
  })
})
