import { expect } from 'chai'

import { testsSetup } from '../../tests.util'
import userRolesService from '../../../../app/components/user-roles/user-roles.service'
import { expectedRoles } from '../../snapshots/user-roles'
import ids from '../../fixtures-ids'

describe('UserRolesService', () => {
  beforeEach(done => {
    testsSetup(done)
  })

  it('should load by ids', async () => {
    const result = await userRolesService.loadByIds([
      ids.userRoles.admin,
      ids.userRoles.user,
    ])
    expect(result).to.containSubset(expectedRoles)
  })
})
