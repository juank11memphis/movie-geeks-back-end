import { expect } from 'chai'
import mongodb from 'mongodb'
import sinon from 'sinon'

import { testsSetup } from '../../tests.util'
import userMoviesService from '../../../../app/components/user-movies/user-movies.service'
import moviesService from '../../../../app/components/movies/movies.service'
import ids from '../../fixtures-ids'
import {
  expectedRatedMovies,
  expectedUserMoviesUserNull,
  expectedUserMoviesUserAll,
  expectedUserMoviesUserSome,
  expectedPlayingNowMovies,
  expectedSearch,
  expectedWatchlistCurrentUserIdAndComparandUserId,
  expectedCollectionCurrentUserIdAndComparandUserId,
  expectedCollectionAs,
  expectedCollectionAsMainUserNull,
  expectedWatchlistAsMainUserNull,
  expectedWatchlistAs,
} from '../../snapshots/user-movies'
import themoviedbApi from '../../../../app/api/themoviedb/themoviedb.api'
import {
  themoviedbApiPlayingNowResponse,
  themoviedbApiParsedSearchResponse,
} from '../../mock-data/themoviedb.mocks'

const sandbox = sinon.createSandbox()

describe('UserMoviesService', () => {
  beforeEach(done => {
    testsSetup(done)
  })

  afterEach(() => {
    sandbox.restore()
  })

  it('should get userMovie records by userId and movieId', async () => {
    const userMovie = await userMoviesService.getByUserAndMovie(
      ids.users.juanId,
      ids.movies.mementoId,
    )
    expect(userMovie).to.containSubset({
      user: ids.users.juanId,
      movie: ids.movies.mementoId,
      saved: true,
    })
  })

  it('should create new userMovie records', async () => {
    const userMovie = await userMoviesService.create({
      user: ids.users.sanId,
      movie: ids.movies.fightClubId,
      rating: 10,
    })
    expect(userMovie).to.containSubset({
      user: ids.users.sanId,
      movie: ids.movies.fightClubId,
      saved: false,
      rating: 10,
    })
  })

  it('should update existing userMovie records', async () => {
    const dbUserMovie = await userMoviesService.getById(
      ids.userMovies.juancaMementoId,
    )
    const dbUpdatedUserMovie = await userMoviesService.update(dbUserMovie, {
      saved: false,
    })
    expect(dbUpdatedUserMovie).to.containSubset({
      user: ids.users.juanId,
      movie: ids.movies.mementoId,
      saved: false,
    })
  })

  it('should throw error if trying to add an unexisting movie to the watchlist', async () => {
    const badMovieId = mongodb.ObjectID()
    try {
      await userMoviesService.addToWatchlist(ids.users.juanId, badMovieId)
    } catch (error) {
      expect(error.message).to.deep.equal(
        `Movie with id ${badMovieId} does not exists`,
      )
    }
  })

  it('should throw error if trying to add a movie to the watchlist with an unexisting user', async () => {
    const badId = mongodb.ObjectID()
    try {
      await userMoviesService.addToWatchlist(badId, ids.movies.fightClubId)
    } catch (error) {
      expect(error.message).to.deep.equal(
        `User with id ${badId} does not exists`,
      )
    }
  })

  it('should add a movie to the watchlist -> Existing movie and userMovie', async () => {
    const userMovie = await userMoviesService.addToWatchlist(
      ids.users.sanId,
      ids.movies.fightClubId,
    )
    expect(userMovie).to.containSubset({
      user: ids.users.sanId,
      movie: ids.movies.fightClubId,
      saved: true,
    })
    const { userMovies: watchlist } = await userMoviesService.loadWatchlist(
      ids.users.sanId,
    )
    expect(watchlist.length).to.equal(1)
  })

  it('should add a movie to the watchlist -> Existing movie but not existing userMovie', async () => {
    const userMovie = await userMoviesService.addToWatchlist(
      ids.users.sanId,
      ids.movies.mementoId,
    )
    expect(userMovie).to.containSubset({
      user: ids.users.sanId,
      movie: ids.movies.mementoId,
      saved: true,
    })

    const { userMovies: watchlist } = await userMoviesService.loadWatchlist(
      ids.users.sanId,
    )
    expect(watchlist.length).to.equal(1)
  })

  it('should throw error if trying to remove a movie from watchlist with an unexisting user or movie', async () => {
    const badId = mongodb.ObjectID()
    try {
      await userMoviesService.removeFromWatchlist(badId, badId)
    } catch (error) {
      expect(error.message).to.deep.equal(`Invalid user or movie`)
    }
  })

  it('should remove movies from watchlist', async () => {
    await userMoviesService.addToWatchlist(
      ids.users.sanId,
      ids.movies.fightClubId,
    )
    await userMoviesService.addToWatchlist(
      ids.users.sanId,
      ids.movies.machinistId,
    )
    let { userMovies: watchlist } = await userMoviesService.loadWatchlist(
      ids.users.sanId,
    )
    expect(watchlist.length).to.equal(2)

    const userMovie = await userMoviesService.removeFromWatchlist(
      ids.users.sanId,
      ids.movies.machinistId,
    )
    expect(userMovie).to.containSubset({
      user: ids.users.sanId,
      movie: ids.movies.machinistId,
      saved: false,
    })

    const { userMovies: watchlist2 } = await userMoviesService.loadWatchlist(
      ids.users.sanId,
    )
    expect(watchlist2.length).to.equal(1)
  })

  it('should load watchlist without filters', async () => {
    const { userMovies: watchlist } = await userMoviesService.loadWatchlist(
      ids.users.juanId,
    )
    expect(watchlist.length).to.equal(4)
    expect(watchlist[0].theMovie.title).to.equal('Snatch')
    expect(watchlist[1].theMovie.title).to.equal('The Machinist')
    expect(watchlist[2].theMovie.title).to.equal('Memento')
    expect(watchlist[3].theMovie.title).to.equal('Seven')
  })

  it('should load a paginated watchlist without filters', async () => {
    const {
      userMovies: first,
      pagingMetadata: firstPaging,
    } = await userMoviesService.loadWatchlist(ids.users.juanId, null, {
      paging: { limit: 1 },
    })
    expect(firstPaging).to.containSubset({
      hasNext: true,
      hasPrevious: false,
    })
    expect(first[0].theMovie.title).to.deep.equal('Snatch')

    const {
      userMovies: second,
      pagingMetadata: secondPaging,
    } = await userMoviesService.loadWatchlist(ids.users.juanId, null, {
      paging: { limit: 1, next: firstPaging.next },
    })
    expect(second[0].theMovie.title).to.deep.equal('The Machinist')

    const {
      userMovies: third,
      pagingMetadata: thirdPaging,
    } = await userMoviesService.loadWatchlist(ids.users.juanId, null, {
      paging: { limit: 1, next: secondPaging.next },
    })
    expect(third[0].theMovie.title).to.deep.equal('Memento')

    const {
      userMovies: fourth,
      pagingMetadata: fourthPaging,
    } = await userMoviesService.loadWatchlist(ids.users.juanId, null, {
      paging: { limit: 1, next: thirdPaging.next },
    })
    expect(fourthPaging).to.containSubset({
      hasNext: false,
      hasPrevious: true,
    })
    expect(fourth[0].theMovie.title).to.deep.equal('Seven')
  })

  it('should load watchlist using genres filters', async () => {
    const { userMovies: watchlist } = await userMoviesService.loadWatchlist(
      ids.users.juanId,
      null,
      {
        filters: {
          genres: ['Mystery', 'Drama'],
        },
      },
    )
    expect(watchlist.length).to.equal(3)
    expect(watchlist[0].theMovie.title).to.equal('The Machinist')
    expect(watchlist[1].theMovie.title).to.equal('Memento')
    expect(watchlist[2].theMovie.title).to.equal('Seven')
  })

  it('should load watchlist using years range filter', async () => {
    const { userMovies: watchlist } = await userMoviesService.loadWatchlist(
      ids.users.juanId,
      null,
      {
        filters: {
          yearsRange: [1995, 2000],
        },
      },
    )
    expect(watchlist.length).to.equal(2)
    expect(watchlist[0].theMovie.title).to.equal('Memento')
    expect(watchlist[1].theMovie.title).to.equal('Seven')
  })

  it('should load watchlist using genres and years range filters together', async () => {
    const { userMovies: watchlist } = await userMoviesService.loadWatchlist(
      ids.users.juanId,
      null,
      {
        filters: {
          genres: ['Crime', 'Drama'],
          yearsRange: [1995, 2011],
        },
      },
    )
    expect(watchlist.length).to.equal(2)
    expect(watchlist[0].theMovie.title).to.equal('Snatch')
    expect(watchlist[1].theMovie.title).to.equal('Seven')
  })

  it('should load watchlist using genres and years range filters and paging params together', async () => {
    const filters = {
      genres: ['Crime', 'Drama'],
      yearsRange: [1995, 2011],
    }
    const {
      userMovies,
      pagingMetadata,
    } = await userMoviesService.loadWatchlist(ids.users.juanId, null, {
      filters,
      paging: {
        limit: 1,
      },
    })
    expect(userMovies.length).to.equal(1)
    expect(userMovies[0].theMovie.title).to.equal('Snatch')
    expect(pagingMetadata).to.containSubset({
      hasPrevious: false,
      hasNext: true,
    })
    const {
      userMovies: nextUserMovies,
      pagingMetadata: nextPagingMetadata,
    } = await userMoviesService.loadWatchlist(ids.users.juanId, null, {
      filters,
      paging: {
        limit: 1,
        next: pagingMetadata.next,
        previous: pagingMetadata.previous,
      },
    })
    expect(nextUserMovies.length).to.equal(1)
    expect(nextUserMovies[0].theMovie.title).to.equal('Seven')
    expect(nextPagingMetadata).to.containSubset({
      hasPrevious: true,
      hasNext: false,
    })
  })

  it('should rate a movie and remove it from watchlist: movie with no prior ratings data', async () => {
    const dbUserMovie = await userMoviesService.rateMovie(
      ids.users.juanId,
      ids.movies.mementoId,
      10,
    )
    const dbMovie = await moviesService.getById(ids.movies.mementoId)
    expect(dbMovie.ratingsData).to.deep.equal({
      ratingsSum: 10,
      ratingsCount: 1,
      globalRating: 10,
    })

    expect(dbUserMovie.rating).to.equal(10)

    const { userMovies: watchlist } = await userMoviesService.loadWatchlist(
      ids.users.juanId,
    )
    expect(watchlist).to.have.lengthOf(3)
  })

  it('should rate a movie: movie with existing ratings data, new user rating', async () => {
    const dbUserMovie = await userMoviesService.rateMovie(
      ids.users.juanId,
      ids.movies.sevenId,
      9,
    )
    const movie = await moviesService.getById(ids.movies.sevenId)
    expect(movie.ratingsData).to.deep.equal({
      ratingsSum: 67,
      ratingsCount: 8,
      globalRating: 8.375,
    })

    expect(dbUserMovie.rating).to.equal(9)
  })

  it('should rate a movie: movie with existing ratings data, updating existing user rating', async () => {
    const dbUserMovie = await userMoviesService.rateMovie(
      ids.users.sanId,
      ids.movies.sevenId,
      10,
    )
    const movie = await moviesService.getById(ids.movies.sevenId)
    expect(movie.ratingsData).to.deep.equal({
      ratingsSum: 59,
      ratingsCount: 7,
      globalRating: 8.428571428571429,
    })

    expect(dbUserMovie.rating).to.equal(10)
  })

  it('should handle rating a movie sending lower than 0 ratings', async () => {
    const dbUserMovie = await userMoviesService.rateMovie(
      ids.users.juanId,
      ids.movies.fightClubId,
      -10,
    )
    const movie = await moviesService.getById(ids.movies.fightClubId)
    expect(movie.ratingsData).to.deep.equal({
      ratingsSum: 0,
      ratingsCount: 1,
      globalRating: 0,
    })

    expect(dbUserMovie.rating).to.equal(0)
  })

  it('should handle rating a movie sending greater than 10 ratings', async () => {
    const dbUserMovie = await userMoviesService.rateMovie(
      ids.users.juanId,
      ids.movies.fightClubId,
      60,
    )
    const movie = await moviesService.getById(ids.movies.fightClubId)
    expect(movie.ratingsData).to.deep.equal({
      ratingsSum: 10,
      ratingsCount: 1,
      globalRating: 10,
    })

    expect(dbUserMovie.rating).to.equal(10)
  })

  it('should handle removing a movie rating when there is no rate to remove', async () => {
    const dbUserMovie = await userMoviesService.removeMovieRating(
      ids.users.juanId,
      ids.movies.sevenId,
    )
    const movie = await moviesService.getById(ids.movies.sevenId)
    expect(movie.ratingsData).to.deep.equal({
      ratingsSum: 58,
      ratingsCount: 7,
      globalRating: 8.28,
    })

    expect(dbUserMovie.rating).to.equal(undefined)
    expect(dbUserMovie.saved).to.equal(true)
    expect(dbUserMovie.user + '').to.equal(ids.users.juanId + '')
    expect(dbUserMovie.movie + '').to.equal(ids.movies.sevenId + '')
  })

  it('should handle removing a movie rating', async () => {
    const dbUserMovie = await userMoviesService.removeMovieRating(
      ids.users.sanId,
      ids.movies.sevenId,
    )
    const movie = await moviesService.getById(ids.movies.sevenId)
    expect(movie.ratingsData).to.deep.equal({
      ratingsSum: 49,
      ratingsCount: 6,
      globalRating: 8.166666666666666,
    })

    expect(dbUserMovie).to.containSubset({
      saved: false,
      user: ids.users.sanId,
      movie: ids.movies.sevenId,
      rating: null,
    })
  })

  it('should load rated movies without filters', async () => {
    const {
      userMovies: dbRatedUserMovies,
    } = await userMoviesService.loadCollection(ids.users.sanId)
    expect(dbRatedUserMovies).to.containSubset(expectedRatedMovies)
    expect(dbRatedUserMovies[0].movie + '').to.deep.equal(
      ids.movies.requiemId + '',
    )
    expect(dbRatedUserMovies[1].movie + '').to.deep.equal(
      ids.movies.sevenId + '',
    )
  })

  it('should load rated movies using genres filters', async () => {
    const {
      userMovies: dbRatedUserMovies,
    } = await userMoviesService.loadCollection(ids.users.sanId, null, {
      filters: {
        genres: ['Crime'],
      },
    })
    expect(dbRatedUserMovies.length).to.equal(2)
    expect(dbRatedUserMovies[0].theMovie.title).to.equal('Seven')
    expect(dbRatedUserMovies[1].theMovie.title).to.equal('Snatch')
  })

  it('should load rated movies using yearsRange filters', async () => {
    const {
      userMovies: dbRatedUserMovies,
    } = await userMoviesService.loadCollection(ids.users.sanId, null, {
      filters: {
        yearsRange: [2001, 2019],
      },
    })
    expect(dbRatedUserMovies.length).to.equal(2)
    expect(dbRatedUserMovies[0].theMovie.title).to.equal('Requiem for a Dream')
    expect(dbRatedUserMovies[1].theMovie.title).to.equal('Snatch')
  })

  it('should load rated movies using minimumRating filter', async () => {
    const {
      userMovies: dbRatedUserMovies,
    } = await userMoviesService.loadCollection(ids.users.sanId, null, {
      filters: {
        minimumRating: 9,
      },
    })
    expect(dbRatedUserMovies.length).to.equal(2)
    expect(dbRatedUserMovies[0].theMovie.title).to.equal('Requiem for a Dream')
    expect(dbRatedUserMovies[1].theMovie.title).to.equal('Seven')
  })

  it('should load rated movies using all filters', async () => {
    const {
      userMovies: dbRatedUserMovies,
    } = await userMoviesService.loadCollection(ids.users.sanId, null, {
      filters: {
        minimumRating: 9,
        yearsRange: [2001, 2019],
        genres: ['Crime'],
      },
    })
    expect(dbRatedUserMovies.length).to.equal(0)

    const {
      userMovies: dbRatedUserMovies2,
    } = await userMoviesService.loadCollection(ids.users.sanId, null, {
      filters: {
        minimumRating: 8,
        yearsRange: [2001, 2019],
        genres: ['Drama'],
      },
    })
    expect(dbRatedUserMovies2.length).to.equal(1)
    expect(dbRatedUserMovies2[0].theMovie.title).to.equal('Requiem for a Dream')
  })

  it('should load paginated rated movies using all filters', async () => {
    const filters = {
      minimumRating: 6,
      yearsRange: [1990, 2019],
      genres: ['Crime', 'Drama'],
    }
    const {
      userMovies,
      pagingMetadata,
    } = await userMoviesService.loadCollection(ids.users.sanId, null, {
      filters,
      paging: {
        limit: 2,
      },
    })
    expect(pagingMetadata).to.containSubset({
      hasPrevious: false,
      hasNext: true,
    })
    expect(userMovies[0].theMovie.title).to.deep.equal('Requiem for a Dream')
    expect(userMovies[1].theMovie.title).to.deep.equal('Seven')

    const {
      userMovies: nextUserMovies,
      pagingMetadata: nextPagingMetadata,
    } = await userMoviesService.loadCollection(ids.users.sanId, null, {
      filters,
      paging: {
        limit: 2,
        next: pagingMetadata.next,
      },
    })

    expect(nextPagingMetadata).to.containSubset({
      hasPrevious: true,
      hasNext: false,
    })
    expect(nextUserMovies.length).to.equal(1)
    expect(nextUserMovies[0].theMovie.title).to.deep.equal('Snatch')
  })

  it('should load userMovies by userId and movies when userId is null', async () => {
    const movies = await moviesService.loadByIds([
      ids.movies.requiemId,
      ids.movies.sevenId,
    ])
    const userMovies = await userMoviesService.getByUserAndMovies(null, movies)
    expect(userMovies).to.containSubset(expectedUserMoviesUserNull)
  })

  it('should load userMovies by userId and movies when user has data for all the movies', async () => {
    const movies = await moviesService.loadByIds([
      ids.movies.sevenId,
      ids.movies.snatchId,
    ])
    const userMovies = await userMoviesService.getByUserAndMovies(
      ids.users.joseId,
      movies,
    )
    expect(userMovies).to.containSubset(expectedUserMoviesUserAll)
  })

  it('should load userMovies by userId and movies when user has data for only some of the movies', async () => {
    const movies = await moviesService.loadByIds([
      ids.movies.sevenId,
      ids.movies.fightClubId,
    ])
    const userMovies = await userMoviesService.getByUserAndMovies(
      ids.users.joseId,
      movies,
    )
    expect(userMovies).to.containSubset(expectedUserMoviesUserSome)
  })

  it('should get playingNowMovies as userMovies', async () => {
    sandbox
      .stub(themoviedbApi, 'getPlayingNow')
      .returns(themoviedbApiPlayingNowResponse)
    const userMovies = await userMoviesService.getPlayingNow(ids.users.juanId)
    expect(userMovies).to.containSubset(expectedPlayingNowMovies)
    expect(userMovies[0].movie).not.to.be.undefined
    expect(userMovies[0].theMovie._id).not.to.be.undefined
    expect(userMovies[1].movie).not.to.be.undefined
    expect(userMovies[1].theMovie._id).not.to.be.undefined
  })

  it('should search and return movies as usermovies', async () => {
    sandbox
      .stub(themoviedbApi, 'searchMovies')
      .returns(themoviedbApiParsedSearchResponse)
    const searchResult = await userMoviesService.search(
      'a-query',
      ids.users.juanId,
    )
    expect(searchResult).to.containSubset(expectedSearch)
  })

  it('should load watchlist when currentUserId and comparandUserId are passed', async () => {
    const { userMovies } = await userMoviesService.loadWatchlist(
      ids.users.juanId,
      ids.users.sanId,
    )
    expect(userMovies).to.containSubset(
      expectedWatchlistCurrentUserIdAndComparandUserId,
    )
  })

  it('should load watchlist when currentUserId is null and comparandUserId is valid', async () => {
    const { userMovies } = await userMoviesService.loadWatchlist(
      null,
      ids.users.joseId,
    )
    expect(userMovies).to.deep.equal([])
  })

  it('should load watchlist as other user when mainUserId is null', async () => {
    const {
      userMovies: dbUserMovies,
    } = await userMoviesService.loadWatchlistAs(null, ids.users.joseId)
    expect(dbUserMovies).to.containSubset(expectedWatchlistAsMainUserNull)
  })

  it('should load watchlist as other user when impersonated user has no movies', async () => {
    const {
      userMovies: dbUserMovies,
    } = await userMoviesService.loadWatchlistAs(
      ids.users.joseId,
      ids.users.sanId,
    )
    expect(dbUserMovies).to.deep.equal([])
  })

  it('should load watchlist as other user', async () => {
    const {
      userMovies: dbUserMovies,
    } = await userMoviesService.loadWatchlistAs(
      ids.users.sanId,
      ids.users.joseId,
    )
    expect(dbUserMovies).to.containSubset(expectedWatchlistAs)
  })

  it('should load a paginated watchlist as other user', async () => {
    const {
      userMovies: dbUserMovies,
      pagingMetadata,
    } = await userMoviesService.loadWatchlistAs(
      ids.users.sanId,
      ids.users.joseId,
      {
        paging: { limit: 1 },
      },
    )
    expect(dbUserMovies[0].theMovie.title).to.deep.equal('Snatch')
    expect(pagingMetadata).to.containSubset({
      hasPrevious: false,
      hasNext: true,
    })
    const {
      userMovies: nextUserMovies,
      pagingMetadata: nextPagingMetadata,
    } = await userMoviesService.loadWatchlistAs(
      ids.users.sanId,
      ids.users.joseId,
      {
        paging: { limit: 1, next: pagingMetadata.next },
      },
    )
    expect(nextUserMovies[0].theMovie.title).to.deep.equal('Memento')
    expect(nextPagingMetadata).to.containSubset({
      hasPrevious: true,
      hasNext: false,
    })
  })

  it('should load collection when currentUserId and comparandUserId are passed', async () => {
    const { userMovies } = await userMoviesService.loadCollection(
      ids.users.joseId,
      ids.users.juanId,
    )
    expect(userMovies).to.containSubset(
      expectedCollectionCurrentUserIdAndComparandUserId,
    )
  })

  it('should load collection when currentUserId is null and comparandUserId is valid', async () => {
    const { userMovies } = await userMoviesService.loadCollection(
      null,
      ids.users.joseId,
    )
    expect(userMovies).to.deep.equal([])
  })

  it('should load rated movies as other user when mainUserId is null', async () => {
    const {
      userMovies: dbUserMovies,
    } = await userMoviesService.loadCollectionAs(null, ids.users.sanId)
    expect(dbUserMovies).to.containSubset(expectedCollectionAsMainUserNull)
  })

  it('should load rated movies as other user', async () => {
    const {
      userMovies: dbUserMovies,
    } = await userMoviesService.loadCollectionAs(
      ids.users.joseId,
      ids.users.sanId,
    )
    expect(dbUserMovies).to.containSubset(expectedCollectionAs)
  })

  it('should load a paginated rated movies as other user', async () => {
    const {
      userMovies: dbUserMovies,
      pagingMetadata,
    } = await userMoviesService.loadCollectionAs(
      ids.users.joseId,
      ids.users.sanId,
      {
        paging: { limit: 2 },
      },
    )
    expect(pagingMetadata).to.containSubset({
      hasNext: true,
      hasPrevious: false,
    })
    expect(dbUserMovies[0].theMovie.title).to.deep.equal('Requiem for a Dream')
    expect(dbUserMovies[1].theMovie.title).to.deep.equal('Seven')
    const {
      userMovies: nextUserMovies,
      pagingMetadata: nextPagingMetadata,
    } = await userMoviesService.loadCollectionAs(
      ids.users.joseId,
      ids.users.sanId,
      {
        paging: { limit: 2, next: pagingMetadata.next },
      },
    )
    expect(nextPagingMetadata).to.containSubset({
      hasNext: false,
      hasPrevious: true,
    })
    expect(nextUserMovies[0].theMovie.title).to.deep.equal('Snatch')
    expect(nextUserMovies[1].theMovie.title).to.deep.equal('Memento')
  })

  it('should load rated movies as other user when impersonated user has no movies', async () => {
    const {
      userMovies: dbUserMovies,
    } = await userMoviesService.loadCollectionAs(
      ids.users.joseId,
      ids.users.juanId,
    )
    expect(dbUserMovies).to.deep.equal([])
  })
})
