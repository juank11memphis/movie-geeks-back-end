import { expect } from 'chai'

import { testsSetup } from '../../tests.util'
import userUsersService from '../../../../app/components/user-users/user-users.service'
import userMoviesService from '../../../../app/components/user-movies/user-movies.service'
import ids from '../../fixtures-ids'
import {
  expectedUserUsers,
  expectedUserUsersNewRating,
  expectedUserUsersRatingModified,
  expectedUserUsersRatingDeleted1,
} from '../../snapshots/user-users'
import { processCurrentData } from '../../../../app/components/user-users/currentDataProcessor'

const timeout = ms => new Promise(res => setTimeout(res, ms))

describe('UserUsersService', () => {
  beforeEach(done => {
    testsSetup(done)
  })

  it('should find records by user ids', async () => {
    const result = await userUsersService.getByUsers(
      ids.users.sammetId,
      ids.users.landeId,
    )
    expect(result.toObject()).to.containSubset(expectedUserUsers)

    const result2 = await userUsersService.getByUsers(
      ids.users.landeId,
      ids.users.sammetId,
    )
    expect(result2.toObject()).to.containSubset(expectedUserUsers)
  })

  it('should find records by users and movieId', async () => {
    const result = await userUsersService.getByUsersAndMovie(
      ids.users.sammetId,
      ids.users.landeId,
      ids.movies.draculaId,
    )
    expect(result).to.equal(null)

    const result2 = await userUsersService.getByUsersAndMovie(
      ids.users.sammetId,
      ids.users.landeId,
      ids.movies.bloodDiamondId,
    )
    expect(result2.toObject()).to.containSubset(expectedUserUsers)
  })

  it('should return proper points when calling getRatingPoints', () => {
    let points = userUsersService.getRatingPoints(4, 10)
    expect(points).to.equal(0)

    points = userUsersService.getRatingPoints(4, 3)
    expect(points).to.equal(1)

    points = userUsersService.getRatingPoints(0, 0.5)
    expect(points).to.equal(2)

    points = userUsersService.getRatingPoints(10, 8.5)
    expect(points).to.equal(1)

    points = userUsersService.getRatingPoints(10, 10)
    expect(points).to.equal(2)

    points = userUsersService.getRatingPoints(1, 0)
    expect(points).to.equal(1)
  })

  it('should return users with same movies', async () => {
    await processCurrentData()
    const usersWithSameMovies = await userUsersService.getUsersWithSameMovie(
      ids.users.bruceId,
      ids.movies.greenBookId,
    )
    const first = usersWithSameMovies[0]
    expect(first.user).to.deep.equal(ids.users.bruceId)
    expect(first.comparandUser).to.deep.equal(ids.users.janickId)
    expect(first.score).to.deep.equal(1)
    expect(first.movies[0].id).to.deep.equal(ids.movies.greenBookId)
    const second = usersWithSameMovies[1]
    expect(second.user).to.deep.equal(ids.users.bruceId)
    expect(second.comparandUser).to.deep.equal(ids.users.nickoId)
    expect(second.score).to.deep.equal(0)
    expect(second.movies[0].id).to.deep.equal(ids.movies.greenBookId)
  })

  it('should return users with same movies again', async () => {
    await processCurrentData()
    const usersWithSameMovies = await userUsersService.getUsersWithSameMovie(
      ids.users.janickId,
      ids.movies.sixthSenseId,
    )
    const first = usersWithSameMovies[0]
    expect(first.user).to.deep.equal(ids.users.harrisId)
    expect(first.comparandUser).to.deep.equal(ids.users.janickId)
    expect(first.score).to.deep.equal(2)
    expect(first.movies[0].id).to.deep.equal(ids.movies.sixthSenseId)
    const second = usersWithSameMovies[1]
    expect(second.user).to.deep.equal(ids.users.janickId)
    expect(second.comparandUser).to.deep.equal(ids.users.murrayId)
    expect(second.score).to.deep.equal(2)
    expect(second.movies[0].id).to.deep.equal(ids.movies.sixthSenseId)
  })

  it('should return users with same movies even one more', async () => {
    await processCurrentData()
    const usersWithSameMovies = await userUsersService.getUsersWithSameMovie(
      ids.users.adrianId,
      ids.movies.snowpiercerId,
    )
    expect(usersWithSameMovies).to.deep.equal([])
  })

  it('should update user users scores after rating a new movie', async () => {
    await processCurrentData()
    await userMoviesService.rateMovie(
      ids.users.janickId,
      ids.movies.easternPromisesId,
      9,
    )
    await timeout(500)
    const usersWithSameMovies = await userUsersService.getUsersWithSameMovie(
      ids.users.janickId,
      ids.movies.easternPromisesId,
    )
    expect(usersWithSameMovies).to.containSubset(expectedUserUsersNewRating)
  })

  it('should update user users scores after updating a movie rating', async () => {
    await processCurrentData()
    await userMoviesService.rateMovie(
      ids.users.harrisId,
      ids.movies.sixthSenseId,
      10,
    )
    await timeout(500)
    const usersWithSameMovies = await userUsersService.getUsersWithSameMovie(
      ids.users.harrisId,
      ids.movies.sixthSenseId,
    )
    expect(usersWithSameMovies).to.containSubset(
      expectedUserUsersRatingModified,
    )
  })

  it('should update user users scores after removing a movie rating #1', async () => {
    await processCurrentData()
    await userMoviesService.removeMovieRating(
      ids.users.harrisId,
      ids.movies.easternPromisesId,
    )
    await timeout(500)
    const usersWithSameMovies = await userUsersService.getUsersWithSameMovie(
      ids.users.harrisId,
      ids.movies.easternPromisesId,
    )
    expect(usersWithSameMovies).to.deep.equal([])
    const userUsers = await userUsersService.getByUsers(
      ids.users.harrisId,
      ids.users.murrayId,
    )
    expect(userUsers).to.containSubset(expectedUserUsersRatingDeleted1)
  })

  it('should update user users scores after removing a movie rating #2', async () => {
    await processCurrentData()
    await userMoviesService.removeMovieRating(
      ids.users.bruceId,
      ids.movies.greenBookId,
    )
    await timeout(500)
    const usersWithSameMovies = await userUsersService.getUsersWithSameMovie(
      ids.users.bruceId,
      ids.movies.greenBookId,
    )
    expect(usersWithSameMovies).to.deep.equal([])
    let userUsers = await userUsersService.getByUsers(
      ids.users.bruce,
      ids.users.janickId,
    )
    expect(userUsers).to.equal(null)
    userUsers = await userUsersService.getByUsers(
      ids.users.bruce,
      ids.users.nickoId,
    )
    expect(userUsers).to.equal(null)
  })

  it('should find top similar users', async () => {
    await processCurrentData()
    let similarUsers = await userUsersService.getDetailedTopSimilarUsers(
      ids.users.mauId,
      3,
    )
    expect(similarUsers.data[0].user + '').to.equal(ids.users.aleId + '')
    expect(similarUsers.data[0].score).to.equal(14)

    expect(similarUsers.data[1].comparandUser + '').to.equal(
      ids.users.mishaId + '',
    )
    expect(similarUsers.data[1].score).to.equal(12)

    expect(similarUsers.data[2].comparandUser + '').to.equal(
      ids.users.irisId + '',
    )
    expect(similarUsers.data[2].score).to.equal(7)

    expect(similarUsers.moviesInCommonIds).to.deep.equal([
      ids.movies.highwaymenId + '',
      ids.movies.ipManId + '',
      ids.movies.tripleThreatId + '',
      ids.movies.fiveFeetApartId + '',
      ids.movies.leRoyId + '',
      ids.movies.unplannedId + '',
      ids.movies.vigilanteId + '',
      ids.movies.incendiesId + '',
      ids.movies.eternalId + '',
    ])

    expect(similarUsers.userIds).to.deep.equal([
      ids.users.aleId + '',
      ids.users.mishaId + '',
      ids.users.irisId + '',
    ])

    similarUsers = await userUsersService.getDetailedTopSimilarUsers(
      ids.users.juanId,
      3,
    )
    expect(similarUsers).to.deep.equal({
      data: [],
      moviesInCommonIds: [],
      userIds: [],
    })
  })
})
