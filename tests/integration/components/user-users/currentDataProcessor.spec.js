import { expect } from 'chai'

import { testsSetup } from '../../tests.util'
import { processCurrentData } from '../../../../app/components/user-users/currentDataProcessor'
import userUsersService from '../../../../app/components/user-users/user-users.service'
import ids from '../../fixtures-ids'

describe('UserUsersCurrentDataProcessor', () => {
  beforeEach(done => {
    testsSetup(done)
  })

  it('should process existing data and generate UserUsers records appropriately', async () => {
    await processCurrentData()

    // bruce with murray
    let data = await userUsersService.getByUsers(
      ids.users.bruceId,
      ids.users.murrayId,
    )
    expect(data.score).to.equal(1)
    expect(data.movies).to.deep.equal([
      { id: ids.movies.inceptionId, points: 1 },
    ])

    // bruce with janick
    data = await userUsersService.getByUsers(
      ids.users.bruceId,
      ids.users.janickId,
    )
    expect(data.score).to.equal(1)
    expect(data.movies).to.deep.equal([
      { id: ids.movies.greenBookId, points: 1 },
    ])

    // bruce with nicko
    data = await userUsersService.getByUsers(
      ids.users.bruceId,
      ids.users.nickoId,
    )
    expect(data.score).to.equal(0)
    expect(data.movies).to.deep.equal([
      { id: ids.movies.greenBookId, points: 0 },
    ])

    // harris with bruce
    data = await userUsersService.getByUsers(
      ids.users.bruceId,
      ids.users.harrisId,
    )
    expect(data).to.equal(null)

    // harris with murray
    data = await userUsersService.getByUsers(
      ids.users.harrisId,
      ids.users.murrayId,
    )
    expect(data.score).to.equal(4)
    expect(data.movies).to.deep.equal([
      { id: ids.movies.easternPromisesId, points: 2 },
      { id: ids.movies.sixthSenseId, points: 2 },
    ])

    // mau with misha
    data = await userUsersService.getByUsers(ids.users.mauId, ids.users.mishaId)
    expect(data.score).to.equal(12)
    expect(data.movies.length).to.equal(6)
  })
})
