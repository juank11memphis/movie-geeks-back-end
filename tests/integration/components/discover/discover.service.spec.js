import { expect } from 'chai'

import { testsSetup } from '../../tests.util'
import { processCurrentData } from '../../../../app/components/user-users/currentDataProcessor'
import discoverService from '../../../../app/components/discover/discover.service'
import ids from '../../fixtures-ids'
import { expectedDiscoverResult } from '../../snapshots/discover'

const hasUsers = (userIds, expectedUserIds) => {
  const expectedUserIdsStrings = expectedUserIds.map(id => id + '')
  let result = true
  userIds.forEach(userId => {
    if (expectedUserIdsStrings.indexOf(userId + '') <= -1) {
      result = false
    }
  })
  return result
}

describe('DiscoverService', () => {
  beforeEach(done => {
    testsSetup(done)
  })

  it('should discover user movies', async () => {
    await processCurrentData()
    let discoverResult = await discoverService.discover(ids.users.mauId)
    expect(discoverResult.users).to.deep.equal([
      ids.users.aleId + '',
      ids.users.mishaId + '',
      ids.users.irisId + '',
      ids.users.pepeId + '',
      ids.users.viviId + '',
      ids.users.taraId + '',
    ])
    const discResultItems = discoverResult.items
    expect(discResultItems[0]).to.containSubset(expectedDiscoverResult[0])
    expect(hasUsers(discResultItems[0].users, [ids.users.pepeId])).to.equal(
      true,
    )

    expect(discResultItems[1]).to.containSubset(expectedDiscoverResult[1])
    expect(
      hasUsers(discResultItems[1].users, [ids.users.irisId, ids.users.pepeId]),
    ).to.equal(true)

    expect(discResultItems[2]).to.containSubset(expectedDiscoverResult[2])
    expect(hasUsers(discResultItems[2].users, [ids.users.pepeId])).to.equal(
      true,
    )

    expect(discResultItems[3]).to.containSubset(expectedDiscoverResult[3])
    expect(hasUsers(discResultItems[3].users, [ids.users.viviId])).to.equal(
      true,
    )

    expect(discResultItems[4]).to.containSubset(expectedDiscoverResult[4])
    expect(hasUsers(discResultItems[4].users, [ids.users.pepeId])).to.equal(
      true,
    )

    expect(discResultItems[5]).to.containSubset(expectedDiscoverResult[5])
    expect(
      hasUsers(discResultItems[5].users, [
        ids.users.irisId,
        ids.users.pepeId,
        ids.users.aleId,
      ]),
    ).to.equal(true)

    expect(discResultItems[6]).to.containSubset(expectedDiscoverResult[6])
    expect(
      hasUsers(discResultItems[6].users, [
        ids.users.taraId,
        ids.users.mishaId,
        ids.users.viviId,
      ]),
    ).to.equal(true)

    expect(discResultItems[7]).to.containSubset(expectedDiscoverResult[7])
    expect(
      hasUsers(discResultItems[7].users, [
        ids.users.aleId,
        ids.users.pepeId,
        ids.users.irisId,
      ]),
    ).to.equal(true)

    expect(discResultItems[8]).to.containSubset(expectedDiscoverResult[8])
    expect(
      hasUsers(discResultItems[8].users, [ids.users.viviId, ids.users.taraId]),
    ).to.equal(true)

    expect(discResultItems[9]).to.containSubset(expectedDiscoverResult[9])
    expect(
      hasUsers(discResultItems[9].users, [
        ids.users.aleId,
        ids.users.pepeId,
        ids.users.irisId,
      ]),
    ).to.equal(true)

    expect(discResultItems[10]).to.containSubset(expectedDiscoverResult[10])
    expect(
      hasUsers(discResultItems[10].users, [
        ids.users.taraId,
        ids.users.mishaId,
        ids.users.viviId,
      ]),
    ).to.equal(true)

    expect(discResultItems[11]).to.containSubset(expectedDiscoverResult[11])
    expect(
      hasUsers(discResultItems[11].users, [ids.users.taraId, ids.users.viviId]),
    ).to.equal(true)

    expect(discResultItems[12]).to.containSubset(expectedDiscoverResult[12])
    expect(
      hasUsers(discResultItems[12].users, [
        ids.users.mishaId,
        ids.users.taraId,
        ids.users.viviId,
      ]),
    ).to.equal(true)

    discoverResult = await discoverService.discover(ids.users.murrayId)
    expect(discoverResult).to.deep.equal({ users: [], items: [] })
  })
})
