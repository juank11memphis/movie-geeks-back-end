import sinon from 'sinon'
import { expect } from 'chai'

import { testsSetup } from '../../tests.util'
import authService from '../../../../app/components/auth/auth.service'
import {
  expectedSignInData,
  expectedSignUpData,
  expectedSignUpSocialData,
  expectedRequestPasswordResetData,
  expectedResetPasswordData,
} from '../../snapshots/auth'
import * as MailgunUtil from '../../../../app/util/mailgun.util'
import User from '../../../../app/components/users/user'
import ids from '../../fixtures-ids'

const sandbox = sinon.createSandbox()

describe('AuthService', () => {
  beforeEach(done => {
    sandbox.stub(MailgunUtil, 'sendResetPasswordEmail').returns(true)
    sandbox.stub(MailgunUtil, 'sendWelcomeEmail').returns(true)
    testsSetup(done)
  })

  afterEach(() => {
    sandbox.restore()
  })

  it('should not sign in if provided email is not registered', async () => {
    try {
      await authService.signin('invalid-email@invalid.com', 'some-password')
    } catch (error) {
      expect(error.message).to.equal('This email is not registered')
    }
  })

  it('should not sign in if provided password is not correct', async () => {
    try {
      const response = await authService.signin(
        'juan@gmail.com',
        'some-password',
      )
    } catch (error) {
      expect(error.message).to.equal('This password is not correct')
    }
  })

  it('should signin when valid email and password are provided', async () => {
    const response = await authService.signin('juan@gmail.com', 'test-password')
    expect(response).to.containSubset(expectedSignInData)
    expect(response.token).not.to.be.undefined
  })

  it('should not sign up if provided email already registered', async () => {
    try {
      await authService.signup({
        email: 'juan@gmail.com',
      })
    } catch (error) {
      expect(error.message).to.equal(
        'User validation failed: firstname: First name is required, email: Email already in use',
      )
    }
  })

  it('should not sign up if provided password is invalid', async () => {
    try {
      const response = await authService.signup({
        firstname: 'Juan',
        email: 'new@gmail.com',
        password: 'abc',
      })
    } catch (error) {
      expect(error.message).to.equal(
        'User validation failed: password: Password must be at least 6 characters long',
      )
    }
  })

  it('should sign up if provided user is all good', async () => {
    const response = await authService.signup({
      firstname: 'new firstname',
      lastname: 'new lastname',
      email: 'new@gmail.com',
      password: 'good-password',
    })
    expect(response).to.containSubset(expectedSignUpData)
    expect(response.token).not.to.be.undefined
  })

  it('should sign up a new user using a social account', async () => {
    const response = await authService.authSocial({
      firstname: 'new firstname',
      lastname: 'new lastname',
      email: 'new@gmail.com',
    })
    expect(response).to.containSubset(expectedSignUpSocialData)
    expect(response.token).not.to.be.undefined
  })

  it('should sign up an existing user using a social account', async () => {
    const response = await authService.authSocial({
      firstname: 'Juan',
      lastname: 'Morales',
      email: 'juan@gmail.com',
    })
    expect(response).to.containSubset(expectedSignInData)
    expect(response.token).not.to.be.undefined
  })

  it('should handle not existing email on request password reset', async () => {
    try {
      const response = await authService.requestPasswordReset('bad@email.com')
    } catch (error) {
      expect(error.message).to.equal('This email is not registered')
    }
  })

  it('should handle request password reset', async () => {
    const response = await authService.requestPasswordReset('juan@gmail.com')
    expect(response).to.containSubset(expectedRequestPasswordResetData)
  })

  it('should not reset password if token user does not exists in database', async () => {
    try {
      const newUser = new User()
      const response = await authService.resetPassword(
        newUser._id,
        'test-password',
      )
    } catch (error) {
      expect(error.message).to.equal(
        'An unexpected error occurred while reseting your password',
      )
    }
  })

  it('should not reset password if new password is invalid', async () => {
    try {
      const user = await User.findOne({
        email: 'juan@gmail.com',
      })
      const response = await authService.resetPassword(user._id, 'abc')
    } catch (error) {
      expect(error.message).to.equal(
        'Validation failed: password: Password must be at least 6 characters long',
      )
    }
  })

  it('should reset password successfully', async () => {
    const user = await User.findOne({
      email: 'juan@gmail.com',
    })
    const response = await authService.resetPassword(
      user._id,
      'new-test-password',
    )
    expect(response).to.containSubset(expectedResetPasswordData)
    expect(response.token).not.to.be.undefined
    expect(response.user.password).not.to.be.undefined
    expect(response.user.updatedAt).not.to.be.undefined
  })

  it('should refresh a token', async () => {
    const response = await authService.refreshToken({
      userId: ids.users.juanId,
    })
    expect(response.user._id + '').to.equal(ids.users.juanId + '')
    expect(response.token).not.to.be.undefined
  })
})
