import { expect } from 'chai'
import sinon from 'sinon'

import searchService from '../../../../app/components/search/search.service'
import themoviedbApi from '../../../../app/api/themoviedb/themoviedb.api'
import { expectedSearchResult1 } from '../../snapshots/search'
import { themoviedbApiParsedSearchResponse } from '../../mock-data/themoviedb.mocks'

const sandbox = sinon.createSandbox()

describe('SearchService', () => {
  after(() => {
    sandbox.restore()
  })

  it('should search movies', async () => {
    sandbox
      .stub(themoviedbApi, 'searchMovies')
      .returns(themoviedbApiParsedSearchResponse)
    const searchResult = await searchService.search('fight')
    expect(searchResult).to.containSubset(expectedSearchResult1)
  })

  it('should handle search when query is null or empty string', async () => {
    let searchResult = await searchService.search(null)
    expect(searchResult).to.deep.equal({ movies: [] })
    searchResult = await searchService.search('')
    expect(searchResult).to.deep.equal({ movies: [] })
  })
})
