import ids from '../fixtures-ids'

export const expectedRatedMovies = [
  {
    rating: 10,
    saved: false,
    user: ids.users.sanId,
    movie: ids.movies.requiemId,
  },
  {
    rating: 9,
    saved: false,
    user: ids.users.sanId,
    movie: ids.movies.sevenId,
  },
]

export const expectedJoseCatalog = {
  watchlist: [
    {
      saved: true,
      user: ids.users.joseId,
      movie: ids.movies.mementoId,
    },
    {
      saved: true,
      user: ids.users.joseId,
      movie: ids.movies.snatchId,
    },
  ],
  collection: [
    {
      saved: false,
      _id: ids.userMovies.joseSevenId,
      user: ids.users.joseId,
      movie: ids.movies.sevenId,
      rating: 9,
    },
    {
      saved: false,
      _id: ids.userMovies.joseRequiemId,
      user: ids.users.joseId,
      movie: ids.movies.requiemId,
      rating: 10,
    },
  ],
}

export const expectedUserMoviesUserNull = [
  {
    user: '',
    movie: ids.movies.requiemId,
    rating: null,
    saved: false,
    theMovie: { _id: ids.movies.requiemId },
  },
  {
    user: '',
    movie: ids.movies.sevenId,
    rating: null,
    saved: false,
    theMovie: { _id: ids.movies.sevenId },
  },
]

export const expectedUserMoviesUserAll = [
  {
    user: ids.users.joseId,
    movie: ids.movies.sevenId,
    rating: 9,
    saved: false,
    theMovie: { _id: ids.movies.sevenId },
  },
  {
    user: ids.users.joseId,
    movie: ids.movies.snatchId,
    rating: null,
    saved: true,
    theMovie: { _id: ids.movies.snatchId },
  },
]

export const expectedUserMoviesUserSome = [
  {
    user: ids.users.joseId,
    movie: ids.movies.sevenId,
    rating: 9,
    saved: false,
    theMovie: { _id: ids.movies.sevenId },
  },
  {
    user: ids.users.joseId,
    movie: ids.movies.fightClubId,
    rating: null,
    saved: false,
    theMovie: { _id: ids.movies.fightClubId },
  },
]

export const expectedPlayingNowMovies = [
  {
    user: ids.users.juanId,
    rating: null,
    saved: false,
    theMovie: { title: 'The Fight Club' },
  },
  {
    user: ids.users.juanId,
    rating: null,
    saved: false,
    theMovie: { title: 'The Fight' },
  },
]

export const expectedSearch = {
  page: 1,
  totalResults: 1232,
  totalPages: 62,
  userMovies: [
    {
      user: ids.users.juanId,
      rating: null,
      saved: false,
      theMovie: { title: 'The Fight Club' },
    },
    {
      user: ids.users.juanId,
      rating: null,
      saved: false,
      theMovie: { title: 'The Fight' },
    },
  ],
}

export const expectedWatchlistCurrentUserIdAndComparandUserId = [
  {
    user: ids.users.juanId,
    saved: true,
    theMovie: { title: 'Snatch' },
    comparandUser: {
      userId: ids.users.sanId + '',
      rating: 7,
      saved: false,
    },
  },
  {
    user: ids.users.juanId,
    saved: true,
    theMovie: { title: 'The Machinist' },
    comparandUser: {
      userId: ids.users.sanId + '',
      rating: null,
      saved: false,
    },
  },
  {
    user: ids.users.juanId,
    saved: true,
    theMovie: { title: 'Memento' },
    comparandUser: {
      userId: ids.users.sanId + '',
      rating: 4,
      saved: false,
    },
  },
  {
    user: ids.users.juanId,
    saved: true,
    theMovie: { title: 'Seven' },
    comparandUser: {
      userId: ids.users.sanId + '',
      rating: 9,
      saved: false,
    },
  },
]

export const expectedCollectionCurrentUserIdAndComparandUserId = [
  {
    user: ids.users.joseId,
    saved: false,
    rating: 10,
    theMovie: { title: 'Requiem for a Dream' },
    comparandUser: {
      userId: ids.users.juanId + '',
      rating: null,
      saved: false,
    },
  },
  {
    user: ids.users.joseId,
    saved: false,
    rating: 9,
    theMovie: { title: 'Seven' },
    comparandUser: {
      userId: ids.users.juanId + '',
      rating: null,
      saved: true,
    },
  },
]

export const expectedCollectionAs = [
  {
    user: ids.users.joseId,
    saved: false,
    rating: 10,
    theMovie: { title: 'Requiem for a Dream' },
    comparandUser: {
      userId: ids.users.sanId + '',
      rating: 10,
      saved: false,
    },
  },
  {
    user: ids.users.joseId,
    saved: false,
    rating: 9,
    theMovie: { title: 'Seven' },
    comparandUser: {
      userId: ids.users.sanId + '',
      rating: 9,
      saved: false,
    },
  },
  {
    user: ids.users.joseId,
    saved: true,
    rating: null,
    theMovie: { title: 'Snatch' },
    comparandUser: {
      userId: ids.users.sanId + '',
      rating: 7,
      saved: false,
    },
  },
  {
    user: ids.users.joseId,
    saved: true,
    rating: null,
    theMovie: { title: 'Memento' },
    comparandUser: {
      userId: ids.users.sanId + '',
      rating: 4,
      saved: false,
    },
  },
]

export const expectedCollectionAsMainUserNull = [
  {
    user: '',
    saved: false,
    rating: null,
    theMovie: { title: 'Requiem for a Dream' },
    comparandUser: {
      userId: ids.users.sanId + '',
      rating: 10,
      saved: false,
    },
  },
  {
    user: '',
    saved: false,
    rating: null,
    theMovie: { title: 'Seven' },
    comparandUser: {
      userId: ids.users.sanId + '',
      rating: 9,
      saved: false,
    },
  },
  {
    user: '',
    saved: false,
    rating: null,
    theMovie: { title: 'Snatch' },
    comparandUser: {
      userId: ids.users.sanId + '',
      rating: 7,
      saved: false,
    },
  },
  {
    user: '',
    saved: false,
    rating: null,
    theMovie: { title: 'Memento' },
    comparandUser: {
      userId: ids.users.sanId + '',
      rating: 4,
      saved: false,
    },
  },
]

export const expectedWatchlistAsMainUserNull = [
  {
    user: '',
    saved: false,
    rating: null,
    theMovie: { title: 'Snatch' },
    comparandUser: {
      userId: ids.users.joseId + '',
      rating: undefined,
      saved: true,
    },
  },
  {
    user: '',
    saved: false,
    rating: null,
    theMovie: { title: 'Memento' },
    comparandUser: {
      userId: ids.users.joseId + '',
      rating: undefined,
      saved: true,
    },
  },
]

export const expectedWatchlistAs = [
  {
    user: ids.users.sanId,
    saved: false,
    rating: 7,
    theMovie: { title: 'Snatch' },
    comparandUser: {
      userId: ids.users.joseId + '',
      rating: undefined,
      saved: true,
    },
  },
  {
    user: ids.users.sanId,
    saved: false,
    rating: 4,
    theMovie: { title: 'Memento' },
    comparandUser: {
      userId: ids.users.joseId + '',
      rating: undefined,
      saved: true,
    },
  },
]
