import ids from '../fixtures-ids'

export const expectedDiscoverResult = [
  {
    movieId: ids.movies.petSemataryId,
    averageRating: 10,
    totalRating: 10,
    count: 1,
  },
  {
    movieId: ids.movies.missingLinkId,
    averageRating: 9.75,
    totalRating: 19.5,
    count: 2,
  },
  {
    movieId: ids.movies.usId,
    averageRating: 9.5,
    totalRating: 9.5,
    count: 1,
  },
  {
    movieId: ids.movies.captainFantasticId,
    averageRating: 9.4,
    totalRating: 9.4,
    count: 1,
  },
  {
    movieId: ids.movies.dumboId,
    averageRating: 9,
    totalRating: 9,
    count: 1,
  },
  {
    movieId: ids.movies.headHunterId,
    averageRating: 8.833333333333334,
    totalRating: 26.5,
    count: 3,
  },
  {
    movieId: ids.movies.tellNoOneId,
    averageRating: 8.333333333333334,
    totalRating: 25,
    count: 3,
  },
  {
    movieId: ids.movies.cryptoId,
    averageRating: 8.299999999999999,
    totalRating: 24.9,
    count: 3,
  },
  {
    movieId: ids.movies.theseusId,
    averageRating: 8.25,
    totalRating: 16.5,
    count: 2,
  },
  {
    movieId: ids.movies.breakthroughId,
    averageRating: 7.833333333333333,
    totalRating: 23.5,
    count: 3,
  },
  {
    movieId: ids.movies.asGoodAsItGetsId,
    averageRating: 7.8,
    totalRating: 23.4,
    count: 3,
  },
  {
    movieId: ids.movies.dirtyPrettyThinsId,
    averageRating: 7.7,
    totalRating: 15.4,
    count: 2,
  },
  {
    movieId: ids.movies.livesOfOthersId,
    averageRating: 7.666666666666667,
    totalRating: 23,
    count: 3,
  },
]
