import ids from '../fixtures-ids'

export const expectedSignInData = {
  user: {
    _id: ids.users.juanId,
    firstname: 'Juan',
    lastname: 'Morales',
    email: 'juan@gmail.com',
    password: '$2a$04$8mHfVd43uWR2WGG7wzbMb.GC2.WyOG1KjEMI7dgoElp1i8R5Qhniu',
    hasPassword: true,
    roles: [],
  },
}

export const expectedSignUpData = {
  user: {
    roles: [],
    firstname: 'new firstname',
    lastname: 'new lastname',
    email: 'new@gmail.com',
    hasPassword: true,
  },
}

export const expectedSignUpSocialData = {
  user: {
    firstname: 'new firstname',
    lastname: 'new lastname',
    email: 'new@gmail.com',
    hasPassword: false,
    roles: [],
  },
}

export const expectedRequestPasswordResetData = {
  roles: [],
  _id: ids.users.juanId,
  firstname: 'Juan',
  lastname: 'Morales',
  email: 'juan@gmail.com',
  password: '$2a$04$8mHfVd43uWR2WGG7wzbMb.GC2.WyOG1KjEMI7dgoElp1i8R5Qhniu',
  hasPassword: true,
}

export const expectedResetPasswordData = {
  user: {
    roles: [],
    _id: ids.users.juanId,
    firstname: 'Juan',
    lastname: 'Morales',
    email: 'juan@gmail.com',
    hasPassword: true,
  },
}
