import ids from '../fixtures-ids'

export const expectedJuanUser = {
  roles: [],
  _id: ids.users.juanId,
  firstname: 'Juan',
  lastname: 'Morales',
  email: 'juan@gmail.com',
}

export const expectedJuanUpdated = {
  roles: [],
  _id: ids.users.juanId,
  firstname: 'Carlos',
  lastname: 'Morales',
  email: 'juan@gmail.com',
}
